"""
Simulation driver for the AD9912 DDS.

Author: Leon Riesebos
Year: 2020
"""

import numpy as np

from artiq.language.core import *
from artiq.language.types import *
from artiq.language.units import *

from dax.sim.device import DaxSimDevice
from dax.sim.signal import get_signal_manager


class AD9912(DaxSimDevice):

    def __init__(self, device_manager, **kwargs):
        # Call super
        super(AD9912, self).__init__(device_manager, **kwargs)

        # Register signals
        signal_manager = get_signal_manager()
        self._init = signal_manager.register(self, 'init', bool, size=1)
        self._freq = signal_manager.register(self, 'freq', float)
        self._phase = signal_manager.register(self, 'phase', float)
        self._amp = signal_manager.register(self, 'amp', float)

        # Store configuration and pre-calculated values
        self.ftw_per_hz: float = (1 << 48) / GHz  # 48 bit / 1 GSPS

    @kernel
    def write32(self, data: TInt32):
        raise NotImplementedError

    @kernel
    def write64(self, upper: TInt32, lower: TInt32):
        raise NotImplementedError

    @kernel
    def write_ftw(self, ftw: TInt64):
        raise NotImplementedError

    @kernel
    def write_pow(self, pow_: TInt32):
        raise NotImplementedError

    @kernel
    def write_asf(self, asf: TInt32):
        raise NotImplementedError

    @kernel
    def write_update(self):
        raise NotImplementedError

    @kernel
    def init(self):
        self._init.push(1)

    @portable(flags={'fast-math'})
    def frequency_to_ftw(self, frequency: TFloat) -> TInt64:
        return np.int64(round(float(frequency * self.ftw_per_hz)))

    @portable(flags={'fast-math'})
    def ftw_to_frequency(self, ftw: TInt64) -> TFloat:
        return ftw / self.ftw_per_hz

    @portable(flags={'fast-math'})
    def turns_to_pow(self, turns: TFloat) -> TInt32:
        return np.int32(round(float((1 << 14) * turns)))

    @portable(flags={'fast-math'})
    def pow_to_turns(self, pow_: TInt32) -> TFloat:
        return pow_ / (1 << 14)

    @portable(flags={'fast-math'})
    def amplitude_to_asf(self, amplitude: TFloat) -> TInt32:
        return np.int32(round(float(0x3FF * amplitude)))

    @portable(flags={'fast-math'})
    def asf_to_amplitude(self, asf: TInt32) -> TFloat:
        return asf / 0x3FF

    @kernel
    def set_mu(self, ftw: TInt64, pow_: TInt32 = 0x0):
        self.set(self.ftw_to_frequency(ftw), self.pow_to_turns(pow_))

    @kernel
    def set_frequency_mu(self, ftw: TInt64):
        self.set_frequency(self.ftw_to_frequency(ftw))

    @kernel
    def set_phase_mu(self, pow_: TInt32):
        self.set_phase(self.pow_to_turns(pow_))

    @kernel
    def set_amplitude_mu(self, asf: TInt32):
        self.set_amplitude(self.asf_to_amplitude(asf))

    @kernel
    def set(self, frequency: TFloat, phase: TFloat = 0.0):
        self.set_frequency(frequency)
        self.set_phase(phase)

    @kernel
    def set_frequency(self, frequency: TFloat):
        self._freq.push(frequency)

    @kernel
    def set_phase(self, phase: TFloat):
        self._phase.push(phase)

    @kernel
    def set_amplitude(self, amplitude: TFloat):
        self._amp.push(amplitude)
