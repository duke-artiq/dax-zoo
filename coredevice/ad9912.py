"""
Core device driver for the AD9912 DDS directly connected to an SPI and TTL channel.

Author: Leon Riesebos
Year: 2020
"""

import numpy as np

from artiq.coredevice import spi2 as spi
from artiq.language.core import kernel, portable
from artiq.language.types import TFloat, TInt32, TInt64
from artiq.language.units import *
from artiq.master.worker_db import DeviceManager

# Import registers from ARTIQ coredevice as they are the same
from artiq.coredevice.ad9912_reg import *


class AD9912:
    """Output control of the Analog Devices AD9912 DDS.

    AD9912 DDS is connected over SPI and is interconnected with other devices.
    """

    # Flags to configure the SPI bus, based on the AD9912 datasheet
    _SPI_CONFIG = 0x00

    # From AD9912 datasheet pages 31-32
    # Addresses in memory of the frequency (ftw), phase (pow), and amplitude (fsc)
    # Amplitude = DAC output current
    _AD9912_ADDR_FTW = AD9912_FTW5 << 16
    _AD9912_ADDR_POW = AD9912_POW1 << 16
    _AD9912_ADDR_FSC = AD9912_FSC1 << 16

    # From AD9912 datasheet page 28
    # Commands to write 1, 2, 3 bytes, or a continuous stream.
    _AD9912_WRITE_ONE_CMD = 0b000 << 29
    _AD9912_WRITE_TWO_CMD = 0b001 << 29
    _AD9912_WRITE_THREE_CMD = 0b010 << 29
    _AD9912_WRITE_STREAM_CMD = 0b011 << 29

    # SPI CLK approx (base clk ~125 MHz) / divider
    _SPI_CLK_DIVIDER = 4

    kernel_invariants = {
        "_SPI_CONFIG",
        "_AD9912_ADDR_FTW",
        "_AD9912_ADDR_POW",
        "_AD9912_ADDR_FSC",
        "_AD9912_WRITE_ONE_CMD",
        "_AD9912_WRITE_TWO_CMD",
        "_AD9912_WRITE_THREE_CMD",
        "_AD9912_WRITE_STREAM_CMD",
        "_SPI_CLK_DIVIDER",
        "core",
        "bus",
        "io_update",
        "chip_select",
        "ftw_per_hz",
    }

    def __init__(self, device_manager: DeviceManager,
                 spi_device: str, io_update: str, chip_select: int = 0b1,
                 core_device: str = "core"):
        """Initialize an AD9912 instance without connecting or writing any data.

        If there is only one chip select, it should be set to 0b1 (default value).

        :param device_manager: Set of all available devices
        :param spi_device: Name of the SPI bus this DDS is on
        :param io_update: Name of the TTL device that io_update is connected to
        :param chip_select: Mask of values to drive on the chip select lines during transactions
        :param core_device: Name of the core device
        """
        assert isinstance(chip_select, int), 'Chip select must be of type int'

        # Obtain other devices
        self.core = device_manager.get(core_device)
        self.bus: spi.SPIMaster = device_manager.get(spi_device)
        self.io_update = device_manager.get(io_update)

        # Store configuration and pre-calculated values
        self.chip_select = chip_select
        self.ftw_per_hz: float = (1 << 48) / GHz  # 48 bit / 1 GSPS

    @kernel
    def write32(self, data: TInt32):
        """Write to 32 bit register.

        :param data: Right-aligned binary data to write out over SPI
        """
        self.bus.set_config_mu(self._SPI_CONFIG | spi.SPI_END, 32, self._SPI_CLK_DIVIDER, self.chip_select)
        self.bus.write(data)

    @kernel
    def write64(self, upper: TInt32, lower: TInt32):
        """Write to 64 bit register.

        :param upper: Upper 32 bit right-aligned binary data to write out over SPI
        :param lower: Lower 32 bit right-aligned binary data to write out over SPI
        """
        self.bus.set_config_mu(self._SPI_CONFIG, 32, self._SPI_CLK_DIVIDER, self.chip_select)
        self.bus.write(upper)
        self.bus.set_config_mu(self._SPI_CONFIG | spi.SPI_END, 32, self._SPI_CLK_DIVIDER, self.chip_select)
        self.bus.write(lower)

    @kernel
    def write_ftw(self, ftw: TInt64):
        """Write the frequency tuning word register without pulsing IO update.

        :param ftw: Frequency tuning word (48 bit)
        """
        upper = np.int32(self._AD9912_WRITE_STREAM_CMD | self._AD9912_ADDR_FTW | ((ftw >> 32) & 0xFFFF))
        lower = np.int32(ftw)
        self.write64(upper, lower)

    @kernel
    def write_pow(self, pow_: TInt32):
        """Write the phase offset word register without pulsing IO update.

        :param pow_: Phase offset word (14 bit)
        """
        pow_ &= 0x3FFF
        self.write32(self._AD9912_WRITE_TWO_CMD | self._AD9912_ADDR_POW | pow_)

    @kernel
    def write_asf(self, asf: TInt32):
        """Write the amplitude scaling factor without pulsing IO update.

        This sets the DAC full-scale current register.

        :param asf: Amplitude scaling factor (10 bit)
        """
        asf &= 0x3FF
        self.write32(self._AD9912_WRITE_TWO_CMD | self._AD9912_ADDR_FSC | asf)

    @kernel
    def write_update(self):
        """Pulse the IO update signal to transfer configuration data to control registers."""
        self.io_update.pulse_mu(2 * self.bus.ref_period_mu)

    @kernel
    def init(self):
        """Initialize and configure the DDS.

        Should be called once in the startup kernel.
        """
        pass

    @portable(flags={'fast-math'})
    def frequency_to_ftw(self, frequency: TFloat) -> TInt64:
        """Convert frequency to DDS frequency word.

        :param frequency: Frequency in Hertz
        :return: Frequency tuning word in machine units
        """
        return np.int64(round(float(frequency * self.ftw_per_hz)))

    @portable(flags={'fast-math'})
    def ftw_to_frequency(self, ftw: TInt64) -> TFloat:
        """Convert DDS frequency word to frequency.

        :param ftw: Frequency tuning word in machine units
        :return: Frequency in Hertz
        """
        return ftw / self.ftw_per_hz

    @portable(flags={'fast-math'})
    def turns_to_pow(self, turns: TFloat) -> TInt32:
        """Returns the phase offset word corresponding to the given phase in turns.

        :param turns: Desired phase tuning word in turns
        :return: Phase offset word in machine units
        """
        return np.int32(round(float((1 << 14) * turns)))

    @portable(flags={'fast-math'})
    def pow_to_turns(self, pow_: TInt32) -> TFloat:
        """Return the phase in turns corresponding to a given phase offset word.

        :param pow_: Phase offset word
        :return: Phase in turns
        """
        return pow_ / (1 << 14)

    @portable(flags={'fast-math'})
    def amplitude_to_asf(self, amplitude: TFloat) -> TInt32:
        """Returns the amplitude scaling factor corresponding to the given amplitude.

        :param amplitude: Desired amplitude in range [0, 1]
        :return: Amplitude scaling factor in machine units
        """
        return np.int32(round(float(0x3FF * amplitude)))

    @portable(flags={'fast-math'})
    def asf_to_amplitude(self, asf: TInt32) -> TFloat:
        """Returns the amplitude corresponding to the given amplitude scaling factor.

        :param asf: Amplitude scaling factor in machine units
        :return: Amplitude
        """
        return asf / 0x3FF

    @kernel
    def set_mu(self, ftw: TInt64, pow_: TInt32 = 0x0):
        """Set the frequency and phase of the DDS in machine units.

        :param ftw: Frequency tuning word (48 bit)
        :param pow_: Phase offset word (14 bit)
        """
        # Set registers
        self.write_ftw(ftw)
        self.write_pow(pow_)
        # Pulse IO update
        self.write_update()

    @kernel
    def set_frequency_mu(self, ftw: TInt64):
        """Set the frequency of the DDS in machine units.

        :param ftw: Frequency tuning word (48 bit)
        """
        # Set registers
        self.write_ftw(ftw)
        # Pulse IO update
        self.write_update()

    @kernel
    def set_phase_mu(self, pow_: TInt32):
        """Set the phase of the DDS in machine units.

        :param pow_: Phase offset word (14 bit)
        """
        # Set registers
        self.write_pow(pow_)
        # Pulse IO update
        self.write_update()

    @kernel
    def set_amplitude_mu(self, asf: TInt32):
        """Set the amplitude of the DDS in machine units.

        This sets the DAC full-scale current register which is a live register.

        :param asf: Amplitude scaling factor (10 bit)
        """
        # Set registers
        self.write_asf(asf)
        # No pulse IO update required for live registers

    @kernel
    def set(self, frequency: TFloat, phase: TFloat = 0.0):
        """Set the frequency and phase of the DDS.

        :param frequency: Frequency in Hz
        :param phase: Phase in turns [0, 1)
        """
        self.set_mu(self.frequency_to_ftw(frequency), self.turns_to_pow(phase))

    @kernel
    def set_frequency(self, frequency: TFloat):
        """Set the frequency of the DDS.

        :param frequency: Frequency in Hz
        """
        self.set_frequency_mu(self.frequency_to_ftw(frequency))

    @kernel
    def set_phase(self, phase: TFloat):
        """Set the phase of the DDS.

        :param phase: Phase in turns [0, 1)
        """
        self.set_phase_mu(self.turns_to_pow(phase))

    @kernel
    def set_amplitude(self, amplitude: TFloat):
        """Set the amplitude of the DDS.

        This sets the DAC full-scale current register.

        :param amplitude: Amplitude [0, 1]
        """
        self.set_amplitude_mu(self.amplitude_to_asf(amplitude))
