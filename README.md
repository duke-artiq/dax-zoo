# DAX zoo

This repository contains a collection of DAX modules and services that are not universal or configurable enough to add
to the [DAX](https://gitlab.com/duke-artiq/dax) repository. They are often hardware-specific and having them generalized
might not be desired. Regardless, the modules and services in this repository can often be modified easily to work with
slightly different hardware or just be an inspiration for writing your own modules and services.

In addition to DAX modules and services, this repository also contains other [ARTIQ](https://github.com/m-labs/artiq)
related code such as core device drivers.

## Contributing

We are open to contributions. If you have any interesting code that you would like to add to this zoo, please make a
merge request. Author information, licenses, and other metadata may be added in the header of your source files or in a
separate (readme) file. If you would like to license your contributions, we suggest using an MIT or GPLv2 license.

## License

There is no repository-wide license and files or directories without an explicit license are considered "all rights
reserved".

## Testing

Code is checked with [flake8](https://flake8.pycqa.org/) and
the [flake8-artiq](https://gitlab.com/duke-artiq/flake8-artiq) plugin.

```shell
$ nix-shell
$ flake8
```
