import logging

from dax.experiment import *

from dax.modules.rpc_benchmark import RpcBenchmarkModule
from dax.modules.led import LedModule

from red_chamber.modules.yb171 import Yb171Module
from red_chamber.modules.ablation import AblationModule
from red_chamber.modules.beatnote import BeatnoteModule
from red_chamber.modules.carrier import CarrierModule
from red_chamber.modules.cw_laser import CWModule
from red_chamber.modules.microwave import MicrowaveModule
from red_chamber.modules.mirror import MirrorModule
from red_chamber.modules.pmt import PmtModule
from red_chamber.modules.trap import TrapModule

from red_chamber.services.cool import CoolService
from red_chamber.services.detection import DetectionService
from red_chamber.services.ion_load import IonLoadService
from red_chamber.services.state import StateService
from red_chamber.services.time_resolved import TimeResolvedService

from red_chamber.services.mw_operation import MicrowaveOperationService
from red_chamber.services.mw_operation_sk1 import MicrowaveOperationSK1Service


class RedChamberSystem(DaxSystem):
    SYS_ID = 'red_chamber'
    SYS_VER = 2

    def build(self):
        # Adjust logging level
        self.logger.setLevel(min(self.logger.getEffectiveLevel(), logging.INFO))

        # Call super, obtains core devices
        super(RedChamberSystem, self).build()

        # Add standard modules
        RpcBenchmarkModule(self, 'rpc_bench')
        self.led = LedModule(self, 'led', *[f'led{i:d}' for i in range(2)], init_kernel=False)
        self.update_kernel_invariants('led')

        # Add modules
        self.yb171 = Yb171Module(self, 'yb171')  # Meta-module
        AblationModule(self, 'ablation')  # Not assigned to an attribute
        self.beatnote = BeatnoteModule(self, 'beatnote')
        self.carrier = CarrierModule(self, 'carrier')
        self.cw = CWModule(self, 'cw')
        self.microwave = MicrowaveModule(self, 'microwave')
        self.mirror = MirrorModule(self, 'mirror')
        self.pmt = PmtModule(self, 'pmt')
        self.trap = TrapModule(self, 'trap')
        self.update_kernel_invariants('yb171', 'beatnote', 'carrier', 'cw', 'microwave', 'mirror', 'pmt', 'trap')

        # Add services
        self.cool = CoolService(self)
        self.detection = DetectionService(self)
        self.ion_load = IonLoadService(self)
        self.state = StateService(self)
        self.time_resolved = TimeResolvedService(self)
        self.update_kernel_invariants('cool', 'detection', 'ion_load', 'state', 'time_resolved')
        # Add second-level services
        self.microwave_operation = MicrowaveOperationService(self)
        self.microwave_operation_sk1 = MicrowaveOperationSK1Service(self)
        self.update_kernel_invariants('microwave_operation', 'microwave_operation_sk1')

        # Add other devices
        self.scheduler = self.get_device('scheduler')
        self.update_kernel_invariants('scheduler')

    @kernel
    def init(self):
        """Joint kernel to initialize various modules.

        By manually initializing modules in a single kernel, the number of compiler runs
        can be reduced with faster initialization as a result.
        """
        # Call initialization kernel functions (they include calls to reset() and wait_until_mu()
        self.led.init_kernel()
        self.carrier.init_kernel()
        self.cw.init_kernel()
        self.microwave.init_kernel()
        self.pmt.init_kernel()
