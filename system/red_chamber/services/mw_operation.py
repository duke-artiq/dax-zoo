import typing

import numpy as np

from dax.experiment import *
from dax.interfaces.operation import OperationInterface

from red_chamber.modules.yb171 import Yb171Module
from red_chamber.modules.microwave import MicrowaveModule
from red_chamber.modules.pmt import PmtModule
from red_chamber.services.state import StateService
from red_chamber.services.cool import CoolService
from red_chamber.services.detection import DetectionService


# noinspection PyAbstractClass
class MicrowaveOperationService(DaxService, OperationInterface):
    SERVICE_NAME = 'mw_operation'

    # System dataset keys
    DEFAULT_REALTIME_KEY = 'default_realtime'

    _POW_BITS = 14
    """Number of bits in the phase offset word."""
    _POW_PI = np.int32(1 << (_POW_BITS - 1))
    """Pi as phase offset word."""
    _POW_PI_3_4 = np.int32(round((1 << _POW_BITS) * 0.75))
    """3/4ths Pi as phase offset word."""

    def build(self):
        # Kernel invariant class variables
        self.update_kernel_invariants('_POW_PI', '_POW_PI_3_4')
        # Kernel invariant properties
        self.update_kernel_invariants('pi', 'num_qubits', '_duration_pi_mu', '_channel_map')

        # Get modules and services
        self._yb171 = self.registry.find_module(Yb171Module)
        self._microwave = self.registry.find_module(MicrowaveModule)
        self._pmt = self.registry.find_module(PmtModule)
        self._state = self.registry.get_service(StateService)
        self._cool = self.registry.get_service(CoolService)
        self._detection = self.registry.get_service(DetectionService)
        self.update_kernel_invariants('_yb171', '_microwave', '_pmt', '_state', '_cool', '_detection')

    @host_only
    def init(self) -> None:
        # Realtime flag
        self._realtime: bool = self.get_dataset_sys(self.DEFAULT_REALTIME_KEY, False)
        self.update_kernel_invariants('_realtime')

    @host_only
    def post_init(self) -> None:
        pass

    """Properties and configuration"""

    @property
    def num_qubits(self) -> np.int32:
        return self._yb171.num_ions

    @host_only
    def set_realtime(self, realtime: bool) -> None:
        assert isinstance(realtime, bool), 'Realtime flag must be of type bool'
        self._realtime = realtime

    @property
    def _duration_pi_mu(self) -> np.int64:
        """Pulse duration for a pi pulse in machine units."""
        return self.core.seconds_to_mu(self._microwave.get_duration_pi())

    @property
    def _channel_map(self) -> typing.List[np.int32]:
        """A map to convert qubit index to channel."""
        return self._pmt.active_channels()

    """Operations"""

    @kernel
    def prep_0_all(self):
        # Cool
        self._cool.doppler_pulse_mu()
        # Initialize
        self._state.initialize_pulse()

    @kernel
    def m_z_all(self):
        # Detection
        self._state.detect_active()

    """Measurement handling"""

    @kernel
    def get_measurement(self, qubit: TInt32) -> TInt32:
        return self._detection.measure(self._channel_map[qubit])

    @kernel
    def store_measurements(self, qubits: TList(TInt32)):
        self._state.measure_channels([self._channel_map[q] for q in qubits])

    """Gate functions"""

    @kernel
    def _pulse_mu(self, duration: TInt64, pow_: TInt32):
        """Arbitrary rotation (pulse duration) with arbitrary phase.

        :param duration: Angle to rotate by given as a pulse duration in machine units
        :param pow_: Phase of the MW DDS as a phase offset word
        """
        self._microwave.config_phase_mu(pow_, realtime=self._realtime)
        self._microwave.pulse_mu(duration)

    @kernel(flags={'fast-math'})
    def _rotate_mu(self, theta: TFloat, pow_: TInt32):
        """Arbitrary rotation with arbitrary phase.

        :param theta: Angle to rotate by
        :param pow_: Phase of the MW DDS as a phase offset word
        """
        self._pulse_mu(self.core.seconds_to_mu(theta / self._duration_pi_mu), pow_=pow_)

    @kernel(flags={'fast-math'})
    def rphi(self, theta: TFloat, phi: TFloat, qubit: TInt32 = -1):
        # TODO: the correctness of this function has not been verified yet
        self._rotate_mu(theta, pow_=self._microwave.turns_to_pow(phi / 2 * self.pi))

    @kernel
    def rx(self, theta: TFloat, qubit: TInt32 = -1):
        if theta >= 0.0:
            self._rotate_mu(theta, 0)
        else:
            self._rotate_mu(-theta, self._POW_PI)

    @kernel
    def ry(self, theta: TFloat, qubit: TInt32 = -1):
        if theta >= 0.0:
            self._rotate_mu(theta, self._POW_PI >> 1)
        else:
            self._rotate_mu(-theta, self._POW_PI_3_4)

    @kernel
    def rz(self, theta: TFloat, qubit: TInt32 = -1):
        # Z rotations are not native to Rabi interactions, requires a combination of X and Y rotations.
        # Using Euler angles, we can achieve this by performing the rotation Rx(pi/2)Ry(theta)Rx(-pi/2).
        self.sqrt_x_dag()
        self.ry(theta)
        self.sqrt_x()

    @kernel
    def i(self, qubit: TInt32 = -1):
        pass

    @kernel
    def x(self, qubit: TInt32 = -1):
        self._pulse_mu(self._duration_pi_mu, 0)

    @kernel
    def y(self, qubit: TInt32 = -1):
        self._pulse_mu(self._duration_pi_mu, self._POW_PI >> 1)

    @kernel
    def z(self, qubit: TInt32 = -1):
        self.sqrt_x_dag()
        self.y()
        self.sqrt_x()

    @kernel
    def sqrt_x(self, qubit: TInt32 = -1):
        self._pulse_mu(self._duration_pi_mu >> 1, 0)

    @kernel
    def sqrt_x_dag(self, qubit: TInt32 = -1):
        self._pulse_mu(self._duration_pi_mu >> 1, self._POW_PI)

    @kernel
    def sqrt_y(self, qubit: TInt32 = -1):
        self._pulse_mu(self._duration_pi_mu >> 1, self._POW_PI >> 1)

    @kernel
    def sqrt_y_dag(self, qubit: TInt32 = -1):
        self._pulse_mu(self._duration_pi_mu >> 1, self._POW_PI_3_4)

    @kernel
    def sqrt_z(self, qubit: TInt32 = -1):
        self.sqrt_x_dag()
        self.sqrt_y()
        self.sqrt_x()

    @kernel
    def sqrt_z_dag(self, qubit: TInt32 = -1):
        self.sqrt_x_dag()
        self.sqrt_y_dag()
        self.sqrt_x()

    @kernel
    def h(self, qubit: TInt32 = -1):
        # Hadamard is equivalent to (1/sqrt(2))*(X + Z).
        # However, Z rotation is not native to Rabi oscillations.
        # We can achieve the same result with Rx(pi/2)(1/sqrt(2))*(X + Y)Rx(-pi/2).
        self.sqrt_x_dag()
        self._pulse_mu(self._duration_pi_mu, self._POW_PI >> 2)
        self.sqrt_x()
