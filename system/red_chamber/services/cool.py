from dax.experiment import *

from red_chamber.modules.cw_laser import CWModule
from red_chamber.modules.trap import TrapRfModule


class CoolService(DaxService):
    SERVICE_NAME = 'cool'

    # System dataset keys
    DOPPLER_COOL_TIME_KEY = 'doppler_cool_time'

    def build(self):
        # Get modules
        self._cw = self.registry.find_module(CWModule)
        self._rf = self.registry.find_module(TrapRfModule)
        self.update_kernel_invariants('_cw', '_rf')

    def init(self) -> None:
        self._doppler_cool_time_mu: np.int64 = self.core.seconds_to_mu(
            self.get_dataset_sys(self.DOPPLER_COOL_TIME_KEY, 1 * ms))
        self.update_kernel_invariants('_doppler_cool_time_mu')

    def post_init(self) -> None:
        pass

    """Service functionality"""

    @kernel
    def idle_cooling(self, realtime: TBool = False):
        """Enable idle cooling (Doppler cooling with high amplitude)."""
        self._cw.cool_on(high=True, realtime=realtime)

    @kernel
    def doppler_set(self, state: TBool, eit: TBool = False, fast: TBool = False, realtime: TBool = False,
                    lock_rf: TBool = True):
        """Enable or disable Doppler cooling (asymmetric operation).

        :param state: State to set
        :param eit: :const:`True` to enable EIT cooling
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        :param lock_rf: Lock the RF signal
        """
        self._cw.cool_set(state, eit=eit, fast=fast, realtime=realtime)
        self._rf.set_lock(state and lock_rf)

    @kernel
    def doppler_on(self, eit: TBool = False, fast: TBool = False, realtime: TBool = False,
                   lock_rf: TBool = True):
        """Enable Doppler cooling (asymmetric operation)."""
        self.doppler_set(True, eit=eit, fast=fast, realtime=realtime, lock_rf=lock_rf)

    @kernel
    def doppler_off(self, eit: TBool = False, fast: TBool = False, realtime: TBool = False,
                    lock_rf: TBool = True):
        """Disable Doppler cooling (asymmetric operation)."""
        self.doppler_set(False, eit=eit, fast=fast, realtime=realtime, lock_rf=lock_rf)

    @kernel
    def doppler_pulse_mu(self, duration: TInt64 = 0, eit: TBool = False, fast: TBool = False, realtime: TBool = False,
                         lock_rf: TBool = True):
        """Doppler c for a given period of time (symmetric operation).

        :param duration: The pulse duration in machine units (default duration if not given)
        :param eit: :const:`True` to enable EIT cooling
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        :param lock_rf: Lock the RF signal
        """

        if duration <= 0:
            # Use default cooling time
            duration = self._doppler_cool_time_mu

        try:
            # Switch devices
            self.doppler_on(eit=eit, fast=fast, realtime=realtime, lock_rf=lock_rf)
            delay_mu(duration)
            self.doppler_off(eit=eit, fast=True, realtime=realtime, lock_rf=lock_rf)
        except RTIOUnderflow:
            self.core.break_realtime()
            self.doppler_off()
            self.core.wait_until_mu(now_mu())
            raise

    @kernel
    def doppler_pulse(self, duration: TFloat = 0.0, eit: TBool = False, fast: TBool = False, realtime: TBool = False,
                      lock_rf: TBool = True):
        """Doppler cool for a given period of time (symmetric operation).

        :param duration: The pulse duration (default duration if not given)
        :param eit: :const:`True` to enable EIT cooling
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        :param lock_rf: Lock the RF signal
        """
        self.doppler_pulse_mu(self.core.seconds_to_mu(duration), eit=eit, fast=fast, realtime=realtime, lock_rf=lock_rf)
