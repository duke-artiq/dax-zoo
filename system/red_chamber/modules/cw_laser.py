import typing
import types

import numpy as np

import artiq.coredevice.ttl

from dax.experiment import *
from dax.util.units import time_to_str

import red_chamber.coredevice.ad9912


class Switch(DaxModule):
    """Module for controlling a single switch."""

    SW_TYPE = artiq.coredevice.ttl.TTLOut
    """Switch device type."""

    # System dataset keys
    SW_LATENCY_KEY = 'sw_latency'

    # noinspection PyMethodOverriding
    def build(self, *, sw_key: str, active_low: bool = False) -> None:
        assert isinstance(sw_key, str)
        assert isinstance(active_low, bool)

        # Obtain switch device
        self._sw = self.get_device(sw_key, self.SW_TYPE)
        self.update_kernel_invariants('_sw')

        # Store active low flag
        self._active_low = active_low
        self.update_kernel_invariants('_active_low')

    def init(self) -> None:
        # Get system datasets
        self._sw_latency: float = self.get_dataset_sys(self.SW_LATENCY_KEY, 0.0)
        self.update_kernel_invariants('_sw_latency')

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def set(self, state: TBool):
        """Set the state.

        :param state: :const:`True` to enable this switch
        """
        # Switch with latency compensation
        delay(-self._sw_latency)
        self._sw.set_o(state != self._active_low)
        delay(self._sw_latency)

    @kernel
    def on(self):
        """Enable switch."""
        self.set(True)

    @kernel
    def off(self):
        """Disable switch."""
        self.set(False)

    @kernel
    def pulse_mu(self, duration: TInt64):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in machine units
        """
        # Latency compensation, applied once for the whole operation for efficiency
        delay(-self._sw_latency)

        try:
            self._sw.set_o(True ^ self._active_low)
            delay_mu(duration)
            self._sw.set_o(False ^ self._active_low)
        except RTIOUnderflow:
            self.safety_off()
            raise

        # Undo latency compensation
        delay(self._sw_latency)

    @kernel
    def pulse(self, duration: TFloat):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in seconds
        """
        self.pulse_mu(self.core.seconds_to_mu(duration))

    @kernel
    def safety_off(self):
        """Disable switch in a safe manner.

        This function should not raise exceptions.
        """
        self.core.break_realtime()
        self.off()
        self.core.wait_until_mu()


class CoolPumpDds(Switch):
    # System dataset keys
    COOL_FREQ_KEY = 'cool_freq'
    COOL_ASF_KEY = 'cool_asf'
    COOL_HIGH_ASF_KEY = 'cool_high_asf'
    PUMP_FREQ_KEY = 'pump_freq'
    PUMP_ASF_KEY = 'pump_asf'
    DDS_LATENCY_MU_KEY = 'dds_latency_mu'

    def build(self, **kwargs) -> None:
        # Obtain DDS device
        dds_key = 'dds3_2'
        self._dds = self.get_device(dds_key, red_chamber.coredevice.ad9912.AD9912)
        self.update_kernel_invariants('_dds')

        # Call super
        super(CoolPumpDds, self).build(sw_key=f'{dds_key}_sw', **kwargs)

    def init(self) -> None:
        # Call super
        super(CoolPumpDds, self).init()

        # Get system datasets
        self._cool_freq: float = self.get_dataset_sys(self.COOL_FREQ_KEY, 200 * MHz)
        self._cool_asf: int = self.get_dataset_sys(self.COOL_ASF_KEY, 200)
        self._cool_high_asf: int = self.get_dataset_sys(self.COOL_HIGH_ASF_KEY, 1000)
        self._pump_freq: float = self.get_dataset_sys(self.PUMP_FREQ_KEY, 215 * MHz)
        self._pump_asf: int = self.get_dataset_sys(self.PUMP_ASF_KEY, 1000)
        self._dds_latency_mu: np.int64 = np.int64(self.get_dataset_sys(self.DDS_LATENCY_MU_KEY, 0))
        self.update_kernel_invariants(
            '_cool_freq', '_cool_asf', '_cool_high_asf',
            '_pump_freq', '_pump_asf',
            '_dds_latency_mu'
        )

    """Module functionality"""

    @kernel
    def config_mu(self, ftw: TInt64, asf: TInt32, realtime: TBool = False):
        if realtime:
            # Compensate DDS latency
            delay_mu(np.int64(-self._dds_latency_mu))
        else:
            # Add slack
            delay(200 * us)

        # Set DDS
        self._dds.set_frequency_mu(ftw)
        self._dds.set_amplitude_mu(asf)

    @kernel
    def config(self, freq: TFloat, amp: TFloat, realtime: TBool = False):
        self.config_mu(self.freq_to_ftw(freq), asf=self.amp_to_asf(amp), realtime=realtime)

    @kernel
    def config_cool(self, high: TBool, realtime: TBool = False):
        self.config_mu(
            self.freq_to_ftw(self._cool_freq),
            self._cool_high_asf if high else self._cool_asf,
            realtime=realtime
        )

    @kernel
    def config_cool_high(self, realtime: TBool = False):
        self.config_mu(self.freq_to_ftw(self._cool_freq), self._cool_high_asf, realtime=realtime)

    @kernel
    def config_pump(self, realtime: TBool = False):
        self.config_mu(self.freq_to_ftw(self._pump_freq), self._pump_asf, realtime=realtime)

    @portable
    def freq_to_ftw(self, freq: TFloat) -> TInt64:
        return self._dds.frequency_to_ftw(freq)

    @portable
    def ftw_to_freq(self, ftw: TInt64) -> TFloat:
        return self._dds.ftw_to_frequency(ftw)

    @portable
    def amp_to_asf(self, amp: TFloat) -> TInt32:
        return self._dds.amplitude_to_asf(amp)

    @portable
    def asf_to_amp(self, asf: TInt32) -> TFloat:
        return self._dds.asf_to_amplitude(asf)

    @host_only
    def update_latency_compensation(self) -> None:
        # Reset latency to zero
        self._dds_latency_mu = np.int64(0)
        # Get new latency
        self._dds_latency_mu = self._get_dds_latency_mu()
        assert self._dds_latency_mu >= 0
        # Store new latency
        self.set_dataset_sys(self.DDS_LATENCY_MU_KEY, int(self._dds_latency_mu))
        self.logger.info(f'Updated DDS latency: ' f'{time_to_str(self.core.mu_to_seconds(self._dds_latency_mu))}')

    @kernel
    def _get_dds_latency_mu(self) -> TInt64:
        # Reset core
        self.core.reset()

        # Calculate latency
        delay(200 * us)
        t_start = now_mu()
        self.config_cool(high=True, realtime=True)
        t_latency = now_mu() - t_start

        # Wait until all events are submitted
        self.core.wait_until_mu(now_mu())

        # Return latency
        return t_latency


class AmbiguousStateError(RuntimeError):
    """Raised if the state of the master switch is ambiguous."""
    pass


class MasterSwitchModule(Switch):
    """This module manages the 370 nm master switch."""

    def build(self, *args, **kwargs):
        # Call super
        super(MasterSwitchModule, self).build(*args, **kwargs)

        # Bit register for the state
        self._master_state = np.int32(0)
        # Bit register with state unknown flags
        self._master_state_unknown = np.int32(0b11)  # Number of high bits == number of modules to share over

    """370 nm master switch"""

    @kernel
    def _master_set(self, state: TBool, mask_n: TInt32):
        # Update state unknown register
        self._master_state_unknown &= mask_n

        # Update state register
        if state:
            self._master_state |= ~mask_n
        else:
            self._master_state &= mask_n

        # Verify that we do not have an ambiguous state
        if not self._master_state and bool(self._master_state_unknown):
            # State is ambiguous if any unknown bit is high
            raise AmbiguousStateError('State of the master 370 switch is ambiguous')

        # Set the switch based on the join state
        self.set(bool(self._master_state))

    @kernel
    def cool_pump_set(self, state: TBool):
        """Cool/pump AOM: Set the local state of the 370 master switch.

        :param state: :const:`True` to indicate the switch should be on
        """
        self._master_set(state, ~(0b1 << 0))

    @kernel
    def detection_set(self, state: TBool):
        """Detection AOM: Set the local state of the 370 master switch.

        :param state: :const:`True` to indicate the switch should be on
        """
        self._master_set(state, ~(0b1 << 1))


class CWModule(DaxModule):

    def build(self):
        # Shared device module for 370 nm master switch
        self._master_sw = MasterSwitchModule(self, 'master', sw_key='out7_3')
        self.update_kernel_invariants('_master_sw')

        # Detection switch (input affected by cool/pump switch, output affects EIT path)
        self._detection_sw = Switch(self, 'detection', sw_key='out7_4')
        self.update_kernel_invariants('_detection_sw')

        # Cooling and pumping switch and DDS (affects input to detection/EIT path)
        self._cool_pump_dds = CoolPumpDds(self, 'cool_pump')
        self.update_kernel_invariants('_cool_pump_dds')

        # 2.1 GHz EOM switch for pumping
        self._eom_pump_sw = Switch(self, 'eom_pump', sw_key='out1_0', active_low=True)
        # 14.7 GHz EOM switch for cooling
        self._eom_cool_sw = Switch(self, 'eom_cool', sw_key='out1_1', active_low=True)
        self.update_kernel_invariants('_eom_pump_sw', '_eom_cool_sw')

        # 399 nm ionization switch
        self._ionization_sw = Switch(self, 'ionization', sw_key='out1_6')
        self.update_kernel_invariants('_ionization_sw')

        # EIT cooling switch
        self._eit_sw = Switch(self, 'eit', sw_key='out7_6')
        self.update_kernel_invariants('_eit_sw')

        # All switches
        self._switches_dict: typing.Dict[str, Switch] = {
            'master': self._master_sw,
            'detection': self._detection_sw,
            'cool_pump': self._cool_pump_dds,
            'eom_pump': self._eom_pump_sw,
            'eom_cool': self._eom_cool_sw,
            'ionization': self._ionization_sw,
            'eit': self._eit_sw,
        }
        self._switches_list = list(self._switches_dict.values())
        self.update_kernel_invariants('_switches_list')

    @host_only
    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        if force:
            # Initialize devices
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # For initialization, always reset core first
        self.core.reset()

        # Switch devices
        delay(10 * us)  # To provide space for latency compensation
        self.cool_on(high=True)
        self.detection_off()
        self.ionization_off()

        # Wait until all events have been submitted, always required for initialization
        self.core.wait_until_mu(now_mu())

    @host_only
    def post_init(self):
        pass

    """Helper functions"""

    @host_only
    def get_switches(self) -> typing.Mapping[str, Switch]:
        """Get a mapping from names to switch modules."""
        return types.MappingProxyType(self._switches_dict)

    """Module functionality"""

    @kernel
    def safety_off(self):
        """Disable all switches in a safe manner."""
        for sw in self._switches_list:
            sw.safety_off()

    @kernel
    def detection_set(self, state: TBool):
        """Switch detection laser (asymmetric operation).

        :param state: State to set
        """
        # Master switch
        self._master_sw.detection_set(state)
        # Switch detection laser
        self._detection_sw.set(state)

    @kernel
    def detection_on(self):
        """Enable detection (asymmetric operation)."""
        self.detection_set(True)

    @kernel
    def detection_off(self):
        """Disable detection (asymmetric operation)."""
        self.detection_set(False)

    @kernel
    def cool_set(self, state: TBool, high: TBool = False, eit: TBool = False,
                 fast: TBool = False, realtime: TBool = False):
        """Enable or disable cooling (asymmetric operation).

        :param state: State to set
        :param high: :const:`True` to enable high-power cooling
        :param eit: :const:`True` to enable EIT cooling
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        if state:
            # Configure DDS
            self._cool_pump_dds.config_cool(high=high, realtime=realtime)
        # Master switch
        self._master_sw.cool_pump_set(state)
        # Shared cool/pump switch
        cool_pump_state = state and not eit
        self._cool_pump_dds.set(cool_pump_state)
        # Independent switches
        self._eom_cool_sw.set(cool_pump_state)
        self._eit_sw.set(state and eit)
        if not fast:
            self._eom_pump_sw.off()

    @kernel
    def cool_on(self, high: TBool = False, eit: TBool = False, fast: TBool = False, realtime: TBool = False):
        """Enable cooling (asymmetric operation)."""
        self.cool_set(True, high=high, eit=eit, fast=fast, realtime=realtime)

    @kernel
    def cool_off(self, high: TBool = False, eit: TBool = False, fast: TBool = False, realtime: TBool = False):
        """Disable cooling (asymmetric operation)."""
        self.cool_set(False, high=high, eit=eit, fast=fast, realtime=realtime)

    @kernel
    def cool_pulse_mu(self, duration: TInt64, high: TBool = False, eit: TBool = False,
                      fast: TBool = False, realtime: TBool = False):
        """Cool for a given period of time (symmetric operation).

        :param duration: The pulse duration in machine units
        :param high: :const:`True` to enable high-power cooling
        :param eit: :const:`True` to enable EIT cooling
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        try:
            # Switch devices
            self.cool_on(high=high, eit=eit, fast=fast, realtime=realtime)
            delay_mu(duration)
            self.cool_off(high=high, eit=eit, fast=True, realtime=realtime)
        except RTIOUnderflow:
            self.core.break_realtime()
            self.cool_off()
            self.core.wait_until_mu(now_mu())
            raise

    @kernel
    def cool_pulse(self, duration: TFloat, high: TBool = False, eit: TBool = False,
                   fast: TBool = False, realtime: TBool = False):
        """Cool for a given period of time (symmetric operation).

        :param duration: The pulse duration
        :param high: :const:`True` to enable high-power cooling
        :param eit: :const:`True` to enable EIT cooling
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.cool_pulse_mu(self.core.seconds_to_mu(duration), high=high, eit=eit, fast=fast, realtime=realtime)

    @kernel
    def initialize_set(self, state: TBool, fast: TBool = False, realtime: TBool = False):
        """Enable or disable state initialization beam (asymmetric operation).

        :param state: State to set
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        if state:
            # Configure DDS
            self._cool_pump_dds.config_pump(realtime=realtime)
        # Master switch
        self._master_sw.cool_pump_set(state)
        # Shared cool/pump switch
        self._cool_pump_dds.set(state)
        # Independent switches
        if not fast:
            self._eom_cool_sw.off()
            self._eit_sw.off()
        self._eom_pump_sw.set(state)

    @kernel
    def initialize_on(self, fast: TBool = False, realtime: TBool = False):
        """Enable state initialization beam."""
        self.initialize_set(True, fast=fast, realtime=realtime)

    @kernel
    def initialize_off(self, fast: TBool = False, realtime: TBool = False):
        """Disable state initialization beam."""
        self.initialize_set(False, fast=fast, realtime=realtime)

    @kernel
    def initialize_pulse_mu(self, duration: TInt64, fast: TBool = False, realtime: TBool = False):
        """State initialization pulse for a given period of time (symmetric operation).

        :param duration: Pulse time in machine units
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        try:
            # Switch devices on normally
            self.initialize_on(fast=fast, realtime=realtime)
            # Delay for pulse duration
            delay_mu(duration)
            # Switch all devices off
            self.initialize_off(fast=True, realtime=realtime)
        except RTIOUnderflow:
            self.core.break_realtime()
            self.initialize_off()
            self.core.wait_until_mu(now_mu())
            raise

    @kernel
    def initialize_pulse(self, duration: TFloat, fast: TBool = False, realtime: TBool = False):
        """State initialization pulse for a given period of time (symmetric operation).

        :param duration: Pulse time
        :param fast: Skip asymmetric switching, assumes a known entry state
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.initialize_pulse_mu(self.core.seconds_to_mu(duration), fast=fast, realtime=realtime)

    @kernel
    def ionization_set(self, state: TBool):
        """Enable or disable ionization (asymmetric operation).

        :param state: State to set
        """
        self._ionization_sw.set(state)

    @kernel
    def ionization_on(self):
        """Enable ionization (asymmetric operation)."""
        self.ionization_set(True)

    @kernel
    def ionization_off(self):
        """Disable ionization (asymmetric operation)."""
        self.ionization_set(False)

    @kernel
    def ionization_pulse_mu(self, duration: TInt64):
        """Ionize for a given period of time (symmetric operation).

        :param duration: The pulse duration in machine units
        """
        try:
            # Switch devices
            self.ionization_on()
            delay_mu(duration)
            self.ionization_off()
        except RTIOUnderflow:
            self.core.break_realtime()
            self.ionization_off()
            self.core.wait_until_mu(now_mu())
            raise

    @kernel
    def ionization_pulse(self, duration: TFloat):
        """Ionize for a given period of time (symmetric operation).

        :param duration: The pulse duration
        """
        self.ionization_pulse_mu(self.core.seconds_to_mu(duration))

    @host_only
    def update_latency_compensation(self) -> None:
        """Update latency compensations for all submodules."""
        self._cool_pump_dds.update_latency_compensation()
