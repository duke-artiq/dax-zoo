import artiq.coredevice.ttl

from dax.experiment import *

import red_chamber.coredevice.sandia_dac_core


class TrapRfModule(DaxModule):

    def build(self):
        # Trap RF lock switch
        self._lock_sw = self.get_device('out1_4', artiq.coredevice.ttl.TTLOut)
        self.update_kernel_invariants('_lock_sw')

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def set_lock(self, state: TBool):
        """Set the trap RF lock state.

        :param state: :const:`True` to enable trap RF lock
        """
        self._lock_sw.set_o(state)

    @kernel
    def lock_on(self):
        self.set(True)

    @kernel
    def lock_off(self):
        self.set(False)


class TrapDacModule(DaxModule):

    def build(self):
        # Sandia DAC driver
        self._sandia_dac_core = self.get_device('sandia_dac_core',
                                                red_chamber.coredevice.sandia_dac_core.DACSerialCoredevice)
        self.update_kernel_invariants('_sandia_dac_core')

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass


class TrapModule(DaxModule):

    def build(self):
        # Create submodules
        self.rf = TrapRfModule(self, 'rf')
        self.dac = TrapDacModule(self, 'dac')
        self.update_kernel_invariants('rf', 'dac')

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass
