import typing

import numpy as np

import artiq.coredevice.ttl

from dax.experiment import *
from dax.util.units import time_to_str

import red_chamber.coredevice.ad9912


class MicrowaveModule(DaxModule):
    DEFAULT_QUBIT_FREQ = 42.82177 * MHz
    """The default microwave qubit frequency."""

    # System dataset keys
    QUBIT_FREQ_KEY = 'qubit_freq'
    RABI_FREQ_KEY = 'rabi_freq'
    ASF_KEY = 'asf'
    DDS_PHASE_LATENCY_MU_KEY = 'dds_phase_latency_mu'

    rabi_freq: float
    """Rabi frequency (optional attribute)."""

    def build(self):
        # Microwave DDS and switch
        self._dds = self.get_device('dds6_1', red_chamber.coredevice.ad9912.AD9912)
        self._sw = self.get_device('dds6_1_sw', artiq.coredevice.ttl.TTLOut)
        self.update_kernel_invariants('_dds', '_sw')

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # System datasets for DDS
        self._qubit_freq: float = self.get_dataset_sys(self.QUBIT_FREQ_KEY, self.DEFAULT_QUBIT_FREQ)
        self.setattr_dataset_sys(self.RABI_FREQ_KEY)
        self._asf: int = self.get_dataset_sys(self.ASF_KEY, 500)
        self._dds_phase_latency_mu: np.int64 = np.int64(self.get_dataset_sys(self.DDS_PHASE_LATENCY_MU_KEY, 0))
        self.update_kernel_invariants('_qubit_freq', '_asf', '_dds_phase_latency_mu')

        if force:
            # Initialize devices
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # For initialization, always reset core first
        self.core.reset()

        # Configure switch and reset DDS
        self.off()
        self.reset()

        # Wait until all events have been submitted, always required for initialization
        self.core.wait_until_mu(now_mu())

    def post_init(self):
        pass

    """Module functionality"""

    @kernel
    def set(self, state: TBool):
        """Set the state of the microwave switch.

        :param state: :const:`True` to enable the microwave switch
        """
        self._sw.set_o(state)

    @kernel
    def on(self):
        """Enable the microwave switch."""
        self.set(True)

    @kernel
    def off(self):
        """Disable the microwave switch."""
        self.set(False)

    @kernel
    def safety_off(self):
        """Disable the microwave switch without underflow exceptions."""
        self.core.break_realtime()
        self.off()
        self.core.wait_until_mu(now_mu())

    @kernel
    def pulse_mu(self, duration: TInt64):
        """Apply a microwave pulse with a given duration.

        :param duration: The duration of the pulse in machine units
        """
        try:
            # Switch devices
            self.on()
            delay_mu(duration)
            self.off()
        except RTIOUnderflow:
            self.safety_off()
            raise

    @kernel
    def pulse(self, duration: TFloat):
        """Apply a microwave pulse with a given duration.

        :param duration: The duration of the pulse
        """
        self.pulse_mu(self.core.seconds_to_mu(duration))

    @kernel
    def config_mu(self, ftw: TInt64, pow_: TInt32 = 0):
        """Change frequency and phase of microwave DDS.

        :param ftw: Frequency tuning word
        :param pow_: Phase offset word, default = 0
        """
        # Add slack
        delay(200 * us)
        # Set DDS
        self._dds.set_mu(ftw, pow_=pow_)

    @kernel
    def config(self, freq: TFloat, turns: TFloat = 0.0):
        """Change frequency and phase of microwave DDS.

        :param freq: The frequency to set
        :param turns: The phase to set in turns, default = 0.0
        """
        self.config_mu(self.freq_to_ftw(freq), pow_=self.turns_to_pow(turns))

    @kernel
    def reset(self):
        """Reset the DDS to its default frequency (microwave qubit frequency), default amplitude, and zero phase."""
        self.config(self._qubit_freq)
        self._dds.set_amplitude_mu(self._asf)

    @kernel
    def config_phase_mu(self, pow_: TInt32, realtime: TBool = False):
        """Change phase of microwave DDS.

        :param pow_: Phase offset word
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        if realtime:
            # Compensate DDS set latency
            delay_mu(np.int64(-self._dds_phase_latency_mu))
        else:
            # Add slack
            delay(200 * us)

        # Set DDS
        self._dds.set_phase_mu(pow_)

    @kernel
    def config_phase(self, turns: TFloat, realtime: TBool = False):
        """Change phase of microwave DDS.

        :param turns: The phase to set in turns
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.config_phase_mu(self.turns_to_pow(turns), realtime=realtime)

    @kernel
    def reset_phase(self, realtime: TBool = False):
        """Reset the microwave DDS phase.

        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.config_phase_mu(0, realtime=realtime)

    @portable
    def freq_to_ftw(self, freq: TFloat) -> TInt64:
        return self._dds.frequency_to_ftw(freq)

    @portable
    def ftw_to_freq(self, ftw: TInt64) -> TFloat:
        return self._dds.ftw_to_frequency(ftw)

    @portable
    def turns_to_pow(self, turns: TFloat) -> TInt32:
        return self._dds.turns_to_pow(turns)

    @portable
    def pow_to_turns(self, pow_: TInt32) -> TFloat:
        return self._dds.pow_to_turnsl(pow_)

    @host_only
    def update_phase_latency_compensation(self) -> None:
        # Reset latency to zero
        self._dds_phase_latency_mu = np.int64(0)
        # Get new latency
        self._dds_phase_latency_mu = self._get_dds_phase_latency_mu()
        assert self._dds_phase_latency_mu >= 0
        # Store new latency
        self.set_dataset_sys(self.DDS_PHASE_LATENCY_MU_KEY, int(self._dds_phase_latency_mu))
        self.logger.info(f'Updated DDS phase latency: '
                         f'{time_to_str(self.core.mu_to_seconds(self._dds_phase_latency_mu))}')

    @kernel
    def _get_dds_phase_latency_mu(self) -> TInt64:
        # Reset core
        self.core.reset()

        # Calculate latency
        delay(200 * us)
        t_start = now_mu()
        self.config_phase_mu(0, realtime=True)
        t_latency = now_mu() - t_start

        # Wait until all events are submitted
        self.core.wait_until_mu(now_mu())

        # Return latency
        return t_latency

    @host_only
    def get_qubit_freq(self, *, fallback: float = DEFAULT_QUBIT_FREQ) -> float:
        """Get the microwave qubit frequency.

        This function is safe to call at any time.
        """
        assert isinstance(fallback, float)
        return self.get_dataset_sys(self.QUBIT_FREQ_KEY, fallback=fallback)

    @host_only
    def set_qubit_freq(self, freq: float) -> None:
        """Set the microwave qubit frequency."""
        assert isinstance(freq, float)
        assert 0 * MHz < freq < 400 * MHz
        self.set_dataset_sys(self.QUBIT_FREQ_KEY, freq)

    @host_only
    def get_rabi_freq(self, *, fallback: typing.Optional[float] = None) -> float:
        """Get the microwave Rabi frequency.

        This function is safe to call at any time.

        :param fallback: A fallback value in case no Rabi frequency is available
        """
        assert isinstance(fallback, float) or fallback is None
        try:
            return self.get_dataset_sys(self.RABI_FREQ_KEY)
        except KeyError:
            if fallback is None:
                raise
            else:
                return fallback

    @host_only
    def get_duration_pi(self, *, fallback: typing.Optional[float] = None) -> float:
        """Get the microwave pulse duration for a pi rotation.

        This function is safe to call at any time.

        :param fallback: A fallback value in case no Rabi frequency is available
        """
        assert isinstance(fallback, float) or fallback is None
        try:
            return np.pi / (2 * np.pi * self.get_rabi_freq())
        except KeyError:
            if fallback is None:
                raise
            else:
                return fallback

    @host_only
    def set_rabi_freq(self, freq: float) -> None:
        """Set the microwave Rabi frequency."""
        assert isinstance(freq, float)
        assert 0 * MHz < freq < 400 * MHz
        self.set_dataset_sys(self.RABI_FREQ_KEY, freq)
