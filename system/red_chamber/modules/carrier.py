import artiq.coredevice.ttl

from dax.experiment import *

import red_chamber.coredevice.ad9912


class CarrierModule(DaxModule):
    # System dataset keys
    FREQ_KEY = 'freq'
    ASF_KEY = 'asf'

    def build(self):
        self._dds = self.get_device('dds6_2', red_chamber.coredevice.ad9912.AD9912)
        self._sw = self.get_device('dds6_2_sw', artiq.coredevice.ttl.TTLOut)
        self.update_kernel_invariants('_dds', '_sw')

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        self._freq: float = self.get_dataset_sys(self.FREQ_KEY, 207.8 * MHz)
        self._asf: int = self.get_dataset_sys(self.ASF_KEY, 200)
        self.update_kernel_invariants('_freq', '_asf')

        if force:
            # Initialize devices
            self.init_kernel()

    @kernel
    def init_kernel(self):
        self.core.reset()
        self.off()
        self.reset()
        self.core.wait_until_mu(now_mu())

    def post_init(self):
        pass

    """Module functionality"""

    @kernel
    def set(self, state: TBool):
        """Set the state of the switch.

        :param state: :const:`True` to enable the carrier switch
        """
        self._sw.set_o(state)

    @kernel
    def on(self):
        """Enable the switch."""
        self.set(True)

    @kernel
    def off(self):
        """Disable the switch."""
        self.set(False)

    @kernel
    def safety_off(self):
        """Disable the switch without underflow exceptions."""
        self.core.break_realtime()
        self.off()
        self.core.wait_until_mu(now_mu())

    @kernel
    def pulse_mu(self, duration: TInt64):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in machine units
        """
        try:
            # Switch devices
            self.on()
            delay_mu(duration)
            self.off()
        except RTIOUnderflow:
            self.safety_off()
            raise

    @kernel
    def pulse(self, duration: TFloat):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse
        """
        self.pulse_mu(self.core.seconds_to_mu(duration))

    @kernel
    def reset(self):
        """Reset the DDS to its default frequency, default amplitude, and zero phase."""
        delay(200 * us)
        self._dds.set(self._freq)
        self._dds.set_amplitude_mu(self._asf)
