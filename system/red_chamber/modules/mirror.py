import typing

import artiq.coredevice.ttl

from dax.experiment import *


class MirrorModule(DaxModule):
    # System dataset keys
    MIRROR_DELAY_KEY = 'mirror_delay'

    PMT: typing.ClassVar[bool] = False
    """Constant for setting the mirror to PMT."""
    CCD: typing.ClassVar[bool] = not PMT
    """Constant for setting the mirror to CCD."""

    def build(self):
        # Make class variables kernel invariant
        self.update_kernel_invariants('PMT', 'CCD')

        # Mirror switch
        self._sw = self.get_device('out1_3', artiq.coredevice.ttl.TTLOut)
        self.update_kernel_invariants('_sw')

    def init(self) -> None:
        # Get mirror delay time
        self._mirror_delay: float = self.get_dataset_sys(self.MIRROR_DELAY_KEY, 0.5 * s)
        self.update_kernel_invariants('_mirror_delay')

    def post_init(self):
        pass

    """Module functionality"""

    @kernel
    def set(self, state: TBool, wait: TBool = True):
        """Set the state of the mirror (asymmetric operation).

        The attributes :attr:`PMT` and :attr:`CCD` can be
        used to input the correct state.

        :param state: New state of the mirror
        :param wait: Wait for the mirror to flip by inserting a delay
        """
        self._sw.set_o(state)
        if wait:
            delay(self._mirror_delay)

    @kernel
    def set_pmt(self, wait: TBool = True):
        """Set mirror to PMT."""
        self.set(self.PMT, wait=wait)

    @kernel
    def set_ccd(self, wait: TBool = True):
        """Set mirror to CCD."""
        self.set(self.CCD, wait=wait)
