from dax.experiment import *
from dax.modules.safety_context import SafetyContext


class AblationModule(SafetyContext):
    CURRENT_KEY = 'current'
    PULSE_FREQ_KEY = 'pulse_freq'

    def build(self) -> None:
        # Call super
        super(AblationModule, self).build(enter_cb=self._on, exit_cb=self._off)

        # Get the controller
        self._ablation_laser = self.get_device('ablation_laser_controller')

    def init(self) -> None:
        # Get system datasets
        self._current = self.get_dataset_sys(self.CURRENT_KEY, 54)
        self._pulse_freq = self.get_dataset_sys(self.PULSE_FREQ_KEY, 10)

        # Switch ablation laser off in the unlikely event it was on
        self._off()

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @rpc
    def _on(self):
        self.logger.info(f'Configuring ablation laser: current={self._current}, pulse_freq={self._pulse_freq}')
        self._ablation_laser.set_all_parameters(self._current, self._pulse_freq, 0, 0)
        self.logger.info('Enabling ablation laser')
        self._ablation_laser.standby()
        self._ablation_laser.fire()

    @rpc
    def _off(self):
        self.logger.info('Disabling ablation laser')
        self._ablation_laser.stop()
