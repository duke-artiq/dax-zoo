import numpy as np

from dax.experiment import *

from brassboard.modules.pmt import PmtModule
from brassboard.modules.cw_laser import CWModule


class CoolingService(DaxService):
    SERVICE_NAME = 'cooling'

    # System dataset keys
    DOPPLER_COOLING_FREQ_KEY = 'doppler_cooling_freq'
    DOPPLER_COOLING_AMP_KEY = 'doppler_cooling_amp'
    DOPPLER_COOLING_ATT_KEY = 'doppler_cooling_att'
    DOPPLER_COOLING_DURATION_KEY = 'doppler_cooling_duration'
    DOPPLER_COOLING_MONITOR_DURATION_KEY = 'doppler_cooling_monitor_duration'

    SS_COOLING_COOL_FREQ_KEY = 'ss_cooling_cool_freq'
    SS_COOLING_COOL_AMP_KEY = 'ss_cooling_cool_amp'
    SS_COOLING_DETECT_FREQ_KEY = 'ss_cooling_detect_freq'
    SS_COOLING_DETECT_AMP_KEY = 'ss_cooling_detect_amp'
    SS_COOLING_DURATION_KEY = 'ss_cooling_duration'
    SS_COOLING_MONITOR_DURATION_KEY = 'ss_cooling_monitor_duration'

    def build(self):
        # Get the relevant modules
        self._pmt = self.registry.find_module(PmtModule)
        self._cw = self.registry.find_module(CWModule)
        self.update_kernel_invariants('_pmt', '_cw')

    def init(self) -> None:
        # Default doppler cooling freq
        self._doppler_cooling_freq: float = self.get_dataset_sys(self.DOPPLER_COOLING_FREQ_KEY, 100 * MHz)
        # Default power for doppler cooling
        self._doppler_cooling_amp: float = self.get_dataset_sys(self.DOPPLER_COOLING_AMP_KEY, 1.0)
        # Default power for doppler cooling
        self._doppler_cooling_att: float = self.get_dataset_sys(self.DOPPLER_COOLING_ATT_KEY, 0 * dB)
        # default duration for doppler cooling
        self._doppler_cooling_duration_mu: np.int64 = self.core.seconds_to_mu(
            self.get_dataset_sys(self.DOPPLER_COOLING_DURATION_KEY, 1 * ms))
        # default duration for collecting counts during doppler cooling
        self._doppler_cooling_monitor_duration_mu: np.int64 = self.core.seconds_to_mu(
            self.get_dataset_sys(self.DOPPLER_COOLING_MONITOR_DURATION_KEY, 0.1 * ms))

        # Default ss cooling freq
        self._ss_cooling_cool_freq: float = self.get_dataset_sys(self.SS_COOLING_COOL_FREQ_KEY, 100 * MHz)
        # Default power for ss cooling
        self._ss_cooling_cool_amp: float = self.get_dataset_sys(self.SS_COOLING_COOL_AMP_KEY, 0.0)
        # default duration for ss cooling
        self._ss_cooling_duration_mu: np.int64 = self.core.seconds_to_mu(
            self.get_dataset_sys(self.SS_COOLING_DURATION_KEY, 1 * ms))

        # Default ss cooling freq
        self._ss_cooling_detect_freq: float = self.get_dataset_sys(self.SS_COOLING_DETECT_FREQ_KEY, 100 * MHz)
        # Default power for ss cooling
        self._ss_cooling_detect_amp: float = self.get_dataset_sys(self.SS_COOLING_DETECT_AMP_KEY, 0.5)
        # default duration for monitor during ss cooling
        self._ss_cooling_monitor_duration_mu: np.int64 = self.core.seconds_to_mu(
            self.get_dataset_sys(self.SS_COOLING_MONITOR_DURATION_KEY, 0.1 * ms))

        # Add attributes to the kernel invariants
        self.update_kernel_invariants('_doppler_cooling_freq', '_doppler_cooling_amp', '_doppler_cooling_att',
                                      '_doppler_cooling_duration_mu', '_doppler_cooling_monitor_duration_mu',
                                      '_ss_cooling_cool_freq', '_ss_cooling_cool_amp', '_ss_cooling_duration_mu',
                                      '_ss_cooling_detect_freq', '_ss_cooling_detect_amp',
                                      '_ss_cooling_monitor_duration_mu')

    def post_init(self) -> None:
        pass

    """Cooling functions"""

    @kernel
    def doppler_cool(self, monitor: TBool = False):
        self._cw.cool_369.config(self._doppler_cooling_freq, self._doppler_cooling_amp)
        self._cw.cool_369.config_att(self._doppler_cooling_att)
        self._cw.cool_369.on()

        if monitor:
            self._pmt.detect_active_mu(self._doppler_cooling_monitor_duration_mu)
            delay_mu(self._doppler_cooling_duration_mu - self._doppler_cooling_monitor_duration_mu)
        else:
            delay_mu(self._doppler_cooling_duration_mu)

        self._cw.cool_369.off()
        self._cw.cool_369.reset_att()

    @kernel
    def second_stage_cool(self, monitor: TBool = False):
        with parallel:
            self._cw.cool_369.config(self._ss_cooling_cool_freq, self._ss_cooling_cool_amp)
            self._cw.detect_369.config(self._ss_cooling_detect_freq, self._ss_cooling_detect_amp)

        if monitor:
            self._pmt.detect_active_mu(self._ss_cooling_monitor_duration_mu)
            delay_mu(self._ss_cooling_duration_mu - self._ss_cooling_monitor_duration_mu)
        else:
            delay_mu(self._ss_cooling_duration_mu)

        self._cw.cool_369.off()
        self._cw.detect_369.off()
