import numpy as np

from dax.experiment import *
from dax.util.units import time_to_str

import artiq.coredevice.ad9910


class MicrowaveModule(DaxModule):
    # System dataset keys
    MW_FREQ_KEY = 'mw_freq'
    DDS_LATENCY_MU_KEY = 'dds_latency_mu'
    RABI_FREQUENCY_KEY = 'rabi_frequency'

    def build(self) -> None:
        # Obtain devices
        self.mw_dds = self.get_device("urukul1_ch0", artiq.coredevice.ad9910.AD9910)

        # When using get_device() instead of setattr_device(), we need to add devices as kernel invariants manually
        self.update_kernel_invariants('mw_dds')

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # Initialization procedures to be executed at the start of experiments using dax_init()
        # Can be used for loading attributes, device configuration, and more
        self._mw_freq: float = self.get_dataset_sys(self.MW_FREQ_KEY, 42.820 * MHz)
        self._dds_latency_mu: np.int64 = np.int64(self.get_dataset_sys(self.DDS_LATENCY_MU_KEY, 0))

        # Add attributes to the kernel invariants
        self.update_kernel_invariants('_mw_freq', '_dds_latency_mu')

        self.setattr_dataset_sys(self.RABI_FREQUENCY_KEY)

        if force:
            # Initialize devices
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # For initialization, always reset core first
        self.core.reset()

        # Microwave
        self.mw_off()
        self.mw_reset()
        delay(5 * us)
        self.mw_dds.set_att(0 * dB)

        # Wait until all events have been submitted, always required for initialization
        self.core.wait_until_mu(now_mu())

    def post_init(self) -> None:
        # Only required when initialization is dependent on initialization of other modules (e.g. when using DMA)
        pass

    """Module functionality"""

    @kernel
    def mw_pulse_mu(self, duration: TInt64):
        """Apply a microwave pulse of the given duration.

        :param duration: The duration of the pulse in machine units
        """
        self.mw_dds.sw.pulse_mu(duration)

    @kernel
    def mw_pulse(self, duration: TFloat):
        """Apply a microwave pulse of the given duration.

        :param duration: The duration of the pulse
        """
        self.mw_pulse_mu(self.core.seconds_to_mu(duration))

    @kernel
    def mw_set(self, state: TBool):
        """Set the state of the microwave switch.

        :param state: `True` to enable the microwave switch
        """
        self.mw_dds.sw.set_o(state)

    @kernel
    def mw_on(self):
        """Enable the microwave switch."""
        self.mw_set(True)

    @kernel
    def mw_off(self):
        """Disable the microwave switch."""
        self.mw_set(False)

    @kernel
    def mw_config(self, freq: TFloat, phase: TFloat = 0.0, amp: TFloat = 1.0, realtime: TBool = False):
        """Change frequency, phase and amplitude of microwave DDS.

        Note: This operation is slow and does alter the timeline!

        :param freq: The frequency to set
        :param phase: The phase to set, default = 0.0
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.mw_config_mu(ftw=self.mw_dds.frequency_to_ftw(freq),
                          pow_=self.mw_dds.turns_to_pow(phase),
                          asf=self.mw_dds.amplitude_to_asf(amp),
                          realtime=realtime)

    @kernel
    def mw_config_mu(self, ftw: TInt32, pow_: TInt32 = 0, asf: TInt32 = 0x3fff, realtime: TBool = False):
        """Change frequency, phase and amplitude of microwave DDS.

        Note: This operation is slow and does alter the timeline!

        :param ftw: Frequency tuning word
        :param pow_: Phase offset word, default = 0
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        if realtime:
            # Compensate DDS set latency
            delay_mu(np.int64(-self._dds_latency_mu))
        else:
            # Add slack
            delay(200 * us)

        # Set DDS
        self.mw_dds.set_mu(ftw, pow_=pow_, asf=asf)

    @kernel
    def mw_reset(self, realtime: TBool = False):
        """Reset microwave DDS to default frequency, zero phase, and full amplitude.

        Note: This operation is slow and does alter the timeline!

        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.mw_config(self._mw_freq, realtime=realtime)

    @host_only
    def update_latency_compensation(self) -> None:
        # Reset latency to zero
        self._dds_latency_mu = np.int64(0)
        # Get new latency
        self._dds_latency_mu = self._get_dds_latency_mu()
        assert self._dds_latency_mu >= 0
        # Store new latency
        self.set_dataset_sys(self.DDS_LATENCY_MU_KEY, self._dds_latency_mu)
        self.logger.info(f'Updated DDS latency: {time_to_str(self.core.mu_to_seconds(self._dds_latency_mu))}')

    @kernel
    def _get_dds_latency_mu(self) -> TInt64:
        # Reset core
        self.core.reset()

        # Calculate latency
        t_start = now_mu()
        self.mw_reset(realtime=True)
        t_latency = now_mu() - t_start

        # Wait until all events are submitted
        self.core.wait_until_mu(now_mu())

        # Return latency
        return t_latency

    @host_only
    def set_rabi_frequency(self, frequency: float) -> None:
        """Set the MW Rabi frequency.

        :param frequency: The new value
        """
        assert isinstance(frequency, float)
        assert frequency > 0 * Hz
        self.set_dataset_sys(self.RABI_FREQUENCY_KEY, frequency)
