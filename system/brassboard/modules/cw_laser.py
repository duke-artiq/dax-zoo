import typing
import numpy as np

from dax.experiment import *

import artiq.coredevice.ad9910
import artiq.coredevice.ttl


class CWLaserSwitch(DaxModule):
    """Module for a CW laser controlled by a single switch."""

    SW_TYPE = artiq.coredevice.ttl.TTLOut
    """Switch device type."""

    # System dataset keys
    SW_INIT_KEY = 'sw_init'
    SW_STATE_KEY = 'sw_state'
    SW_LATENCY_KEY = 'sw_latency'

    # noinspection PyMethodOverriding
    def build(self, *, sw_key: str) -> None:
        # Obtain switch device
        self.__sw = self.get_device(sw_key, self.SW_TYPE)
        self.update_kernel_invariants('_sw')  # Make the property kernel invariant

    @property
    def _sw(self):
        """The switch device."""
        return self.__sw

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # Get system datasets
        self._sw_init: bool = self.get_dataset_sys(self.SW_INIT_KEY, True)
        self._sw_state: bool = self.get_dataset_sys(self.SW_STATE_KEY, False)
        self._sw_latency: float = self.get_dataset_sys(self.SW_LATENCY_KEY, 0.0)
        self.update_kernel_invariants('_sw_init', '_sw_state', '_sw_latency')

        if force:
            # Initialize devices
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # For initialization, always reset core first
        self.core.reset()

        if self._sw_init:
            # Set switch
            self.set_state(self._sw_state)

        # Sync
        self.core.wait_until_mu(now_mu())

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def set_state(self, state: TBool):
        """Set the state.

        :param state: `True` to enable this beam
        """
        # Switch with latency compensation
        delay(-self._sw_latency)
        self._sw.set_o(state)
        delay(self._sw_latency)

    @kernel
    def pulse_mu(self, duration: TInt64):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in machine units
        """
        # Latency compensation, applied once for the whole operation for efficiency
        delay(-self._sw_latency)

        try:
            self._sw.on()
            delay_mu(duration)
            self._sw.off()
        except RTIOUnderflow:
            self.safety_off()
            self.core.wait_until_mu(now_mu())
            raise

        # Undo latency compensation
        delay(self._sw_latency)

    @kernel
    def pulse(self, duration: TFloat):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in seconds
        """
        self.pulse_mu(self.core.seconds_to_mu(duration))

    @kernel
    def on(self):
        """Enable beam."""
        self.set_state(True)

    @kernel
    def off(self):
        """Disable beam."""
        self.set_state(False)

    @kernel
    def safety_off(self):
        """Disable beam in a safe manner.

        This function should not raise exceptions.
        """
        self.core.break_realtime()
        self.off()
        self.core.wait_until_mu(now_mu())


class CWLaserDDS(CWLaserSwitch):
    """Module for a CW laser controlled by a DDS (which includes an RF switch)."""

    DDS_TYPE = artiq.coredevice.ad9910.AD9910
    """DDS device type."""

    # System dataset keys
    DDS_FREQ_KEY = 'dds_freq'
    DDS_AMP_KEY = 'dds_amp'
    DDS_ATT_KEY = 'dds_att'
    DDS_LATENCY_MU_KEY = 'dds_latency_mu'

    # noinspection PyMethodOverriding
    def build(self, *, dds_key: str, default_freq: float) -> None:
        # Do not call super, use the property instead
        self.update_kernel_invariants('_sw')  # Make the property kernel invariant

        # Obtain DDS device
        self._dds = self.get_device(dds_key, self.DDS_TYPE)
        self.update_kernel_invariants('_dds')

        # Store static attributes
        self._default_freq: float = default_freq

    @property
    def _sw(self):
        return self._dds.sw

    def init(self, **kwargs: typing.Any) -> None:
        # Call super
        super(CWLaserDDS, self).init(**kwargs)

        # Get system datasets
        self._dds_freq: float = self.get_dataset_sys(self.DDS_FREQ_KEY, self._default_freq)
        self._dds_amp: float = self.get_dataset_sys(self.DDS_AMP_KEY, 0.2)
        self._dds_att: float = self.get_dataset_sys(self.DDS_ATT_KEY, 10 * dB)
        self._dds_latency_mu: int = self.get_dataset_sys(self.DDS_LATENCY_MU_KEY, 0)
        self.update_kernel_invariants('_dds_freq', '_dds_amp', '_dds_att', '_dds_latency_mu')

    @kernel
    def init_kernel(self):
        # For initialization, always reset core first
        self.core.reset()

        # Initialize switch and DDS
        self._init_sw_and_dds()

        # Sync
        self.core.wait_until_mu(now_mu())

    @kernel
    def _init_sw_and_dds(self):
        # Set DDS and switch
        if self._sw_init and not self._sw_state:
            self.set_state(self._sw_state)
        self.reset(realtime=False)
        if self._sw_init and self._sw_state:
            self.set_state(self._sw_state)

        # Set att
        delay(50 * us)
        self._dds.set_att(self._dds_att)

    """Module functionality"""

    @kernel
    def config_mu(self, ftw: TInt32, asf: TInt32 = -1,
                  realtime: TBool = False):
        """Configure DDS frequency and amplitude.

        :param ftw: The frequency of the DDS
        :param asf: The amplitude of the DDS
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        if asf < 0.0:
            asf = self.amp_to_asf(self._dds_amp)

        if realtime:
            # Compensate DDS configure latency
            delay_mu(np.int64(-self._dds_latency_mu))
        else:
            # Add slack
            delay(250 * us)

        # TODO: see if there is potential to optimize this call (writing less registers should be faster)
        self._dds.set_mu(ftw, asf=asf)

    @kernel
    def config(self, freq: TFloat, amp: TFloat = -1.0,
               realtime: TBool = False):
        if amp < 0.0:
            asf = -1
        else:
            asf = self.amp_to_asf(amp)
        self.config_mu(self.freq_to_ftw(freq), asf=asf, realtime=realtime)

    @kernel
    def reset(self, realtime: TBool = False):
        """Reset configuration to default settings.

        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.config(self._dds_freq, amp=self._dds_amp, realtime=realtime)

    @kernel
    def config_att_mu(self, att: TInt32):
        """Configures attenuation.

        :param att: Attenuation in machine units (255 minimum attenuation, 0 maximum attenuation (31.5 dB))
        """
        self._dds.set_att_mu(att)

    @kernel
    def config_att(self, att: TFloat):
        """Configures attenuation.

        :param att: Attenuation in dB [0-31.5]
        """
        # Does not call self.config_att_mu() because att to mu conversion functions are not available in ARTIQ 6
        self._dds.set_att(att)

    @kernel
    def reset_att(self):
        """Reset attenuation to the default value"""
        self.config_att(self._dds_att)

    @portable
    def freq_to_ftw(self, freq: TFloat) -> TInt32:
        return self._dds.frequency_to_ftw(freq)

    @portable
    def amp_to_asf(self, amp: TFloat) -> TInt32:
        return self._dds.amplitude_to_asf(amp)

    @host_only
    def update_dds_latency(self) -> None:
        """Update the DDS latency.

        Note: **this operation will reset the DDS to its default configuration!**
        """

        # Reset the latency to zero
        self._dds_latency_mu = 0
        # Obtain current latency
        latency_mu = self._get_dds_latency()
        # Store latency
        self.set_dataset_sys(self.DDS_LATENCY_MU_KEY, latency_mu)

    @kernel
    def _get_dds_latency(self) -> TInt64:
        # Reset core
        self.core.reset()

        # Reset in real-time and capture start and end time
        t_start = now_mu()
        self.reset(realtime=True)
        t_end = now_mu()
        self.core.wait_until_mu(t_end)

        # Calculate and return latency
        return t_end - t_start


class CWLaserComboSwOnly(CWLaserDDS):
    """Module for a CW laser controlled by a DDS (which includes an RF switch) and an additional switch."""

    # noinspection PyMethodOverriding
    def build(self, *, sw_key: str, **kwargs: typing.Any) -> None:
        # Call super
        super(CWLaserComboSwOnly, self).build(**kwargs)

        # Obtain combo (i.e. secondary) switch device
        self._combo_sw = self.get_device(sw_key, self.SW_TYPE)
        self.update_kernel_invariants('_combo_sw')

    @property
    def _sw(self):
        return self._combo_sw

    @kernel
    def init_kernel(self):
        # For initialization, always reset core first
        self.core.reset()

        # Set DDS RF switch always on
        self._dds.sw.on()
        # Initialize switch and DDS
        self._init_sw_and_dds()

        # Sync
        self.core.wait_until_mu(now_mu())


class CWLaserCombo(CWLaserComboSwOnly):
    """Module for a CW laser controlled by a DDS and an additional switch."""

    # System dataset keys
    COMBO_SW_LATENCY_KEY = 'combo_sw_latency'

    def init(self, **kwargs: typing.Any) -> None:
        # Call super
        super(CWLaserComboSwOnly, self).init(**kwargs)

        # Get system datasets
        self._combo_sw_latency: float = self.get_dataset_sys(self.COMBO_SW_LATENCY_KEY, 0.0)
        self.update_kernel_invariants('_combo_sw_latency')

        if self._sw_latency > self._combo_sw_latency:
            self.logger.warning('sw latency > combo sw latency, which can reduce the performance of set_state()')

    @property
    def _sw(self):
        return self._dds.sw

    """Module functionality"""

    @kernel
    def set_state(self, state: TBool):
        # Switch twice with latency compensation
        # This code works best if the combo switch latency >= switch latency
        delay(-self._combo_sw_latency)
        self._combo_sw.set_o(state)
        delay(self._combo_sw_latency - self._sw_latency)
        self._sw.set_o(state)
        delay(self._sw_latency)

    @kernel
    def pulse_mu(self, duration: TInt64):
        # Fall back on set_state() for latency compensation
        try:
            self.on()
            delay_mu(duration)
            self.off()
        except RTIOUnderflow:
            self.safety_off()
            self.core.wait_until_mu(now_mu())
            raise


class CWModule(DaxModule):

    def build(self):
        # Instantiate laser modules
        # Cooling freq is zero by default because it should be set in the cooling module
        self.load_369 = CWLaserDDS(self, 'load_369', dds_key='urukul0_ch0', default_freq=100 * MHz)
        self.cool_369 = CWLaserDDS(self, 'cool_369', dds_key='urukul0_ch1', default_freq=0 * MHz)
        self.detect_369 = CWLaserDDS(self, 'detect_369', dds_key='urukul0_ch2', default_freq=100 * MHz)
        self.shutter_399 = CWLaserCombo(self, 'shutter_399', dds_key='urukul0_ch3', default_freq=100 * MHz,
                                        sw_key='ttl16')
        self.switch_sideband_935 = CWLaserSwitch(self, 'switch_sideband_935', sw_key='ttl20')
        self.update_kernel_invariants('load_369', 'cool_369', 'detect_369', 'shutter_399', 'switch_sideband_935')

        # List of all lasers
        self._laser_list = [self.load_369, self.cool_369, self.detect_369, self.shutter_399, self.switch_sideband_935]
        self.update_kernel_invariants('_laser_list')

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def init_kernel(self):
        """Combined initialization kernel."""
        self.load_369.init_kernel()
        self.cool_369.init_kernel()
        self.detect_369.init_kernel()
        self.shutter_399.init_kernel()
        self.switch_sideband_935.init_kernel()
