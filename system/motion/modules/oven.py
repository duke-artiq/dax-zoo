import typing

from dax.experiment import *
from dax.modules.safety_context import SafetyContext
from dax.util.units import volt_to_str, ampere_to_str


class OvenModule(SafetyContext):
    """The oven module for loading Ca."""

    _TARGET_OUTPUT: typing.ClassVar[str] = 'P6V'
    """PSU output channel."""

    # System dataset keys
    DEFAULT_VOLTAGE_KEY = 'voltage'
    DEFAULT_CURRENT_KEY = 'current'

    def build(self) -> None:
        # call super to initialize module as context
        super(OvenModule, self).build(enter_cb=self._on, exit_cb=self._off)
        # Get the PSU controller
        self._power_supply = self.get_device('oven_psu')

    @host_only
    def init(self) -> None:
        """Initialize this module."""
        # Get system datasets
        self._default_voltage: float = self.get_dataset_sys(self.DEFAULT_VOLTAGE_KEY, 3 * V)
        self._default_current: float = self.get_dataset_sys(self.DEFAULT_CURRENT_KEY, 2.6 * A)

        # make sure oven is switched off and voltage and current values are restored to default
        self._off()

    @host_only
    def post_init(self) -> None:
        pass

    """Module functionality"""

    @rpc
    def _on(self):
        """Turns on the oven by ensuring the power supply outputs are enabled."""
        self._power_supply.enable_outputs(True)

    @rpc
    def _off(self):
        """Turn off the oven."""
        self._power_supply.enable_outputs(False)
        # restore oven defaults
        self.reset()

    @rpc
    def config(self, voltage: TFloat, current: TFloat = None):
        """Adjusts the oven temperature by passing the given voltage and current values to the power supply.
        Can also be used to enter context with the desired non-default values when switched on.

        :param voltage: The voltage that should be config on the output of the power supply. Range is 0 - 6.18V
        :param current: (Optional) The current that should be config on the output of the power supply [0-5.25]A
        """
        assert isinstance(voltage, float), 'Voltage must be a float'
        assert isinstance(current, float) or current is None, 'Current must be a float or None'
        self.logger.debug(f'Setting target output {self._TARGET_OUTPUT} to {volt_to_str(voltage)}'
                          f'{"" if current is None else f", {ampere_to_str(current)}"}')
        self._power_supply.set_output_target(self._TARGET_OUTPUT)
        self._power_supply.apply(self._TARGET_OUTPUT, voltage, current)
        # allows entering context with custom values using config
        return self

    @rpc
    def reset(self):
        """Reset the oven voltage and current to its defaults."""
        self.config(self._default_voltage, self._default_current)

    @rpc
    def get_output_settings(self) -> TTuple([TFloat, TFloat]):
        """Return the voltage and current values of the power supply."""
        voltage, current = self._power_supply.get_output_settings(self._TARGET_OUTPUT)
        self.logger.debug(f'Power supply output settings: {volt_to_str(voltage)}, {ampere_to_str(current)}')
        return voltage, current
