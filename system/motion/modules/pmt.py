import numpy as np
import typing

import artiq.coredevice.ttl
import artiq.coredevice.edge_counter

from dax.experiment import *
from dax.interfaces.detection import DetectionInterface
from dax.modules.safety_context import SafetyContext
from dax.util.units import freq_to_str
from dax.util.ccb import get_ccb_tool


class PmtChannelModule(DaxModule):
    COUNT_PLOT_KEY_FORMAT: typing.ClassVar[str] = 'plot.{scheduler.rid}.pmt.{key}'
    """Dataset key format for count plots."""
    COUNT_ARCHIVE_KEY_FORMAT: typing.ClassVar[str] = 'pmt.{key}.{index}'
    """Dataset key format for count archive."""

    PLOT_GROUP: typing.ClassVar[str] = 'pmt'
    """Group for the applets."""
    COUNT_PLOT_NAME_FORMAT: typing.ClassVar[str] = '{key}'
    """Applet name format for count plot."""
    COUNT_PLOT_DEFAULT_KWARGS: typing.ClassVar[typing.Dict[str, typing.Any]] = {
        'last': True,
        'y_label': 'Counts',
    }
    """Default keyword arguments for count plot."""

    # System dataset keys
    DETECTION_OFFSET_KEY = 'detection_offset'

    # noinspection PyMethodOverriding
    def build(self, *, ttl_key: str, counter_key: typing.Optional[str] = None) -> None:
        # Get devices
        self.ttl = self.get_device(ttl_key, artiq.coredevice.ttl.TTLInOut)
        self.edge_counter = self.get_device(f'{ttl_key}_counter' if counter_key is None else counter_key,
                                            artiq.coredevice.edge_counter.EdgeCounter)
        self.update_kernel_invariants('ttl', 'edge_counter')

        # Get scheduler
        self._scheduler = self.get_device('scheduler')
        # Get CCB tool
        self._ccb = get_ccb_tool(self)

        # Flag for the first append call
        self._first_append: bool = True
        # Count buffer
        self._count_buffer: typing.List[int] = []
        # Local cache for all counts
        self._count_cache: typing.List[typing.List[int]] = []

    @host_only
    def init(self) -> None:
        # Detection offset (background noise)
        self._detection_offset: float = self.get_dataset_sys(self.DETECTION_OFFSET_KEY, 0.0)
        # Update kernel invariants
        self.update_kernel_invariants('_detection_offset')

        # Create plot name and key
        self._count_plot_key: str = self.COUNT_PLOT_KEY_FORMAT.format(scheduler=self._scheduler, key=self.get_name())
        self._count_plot_name: str = self.COUNT_PLOT_NAME_FORMAT.format(key=self.get_name())

    @host_only
    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def gate(self, state: TBool):
        """Low-level function to enter/exit a counting gate."""
        self.edge_counter.set_config(state, False, not state, state)

    @kernel
    def gate_enter(self):
        """Low-level function to enter a counting gate."""
        self.gate(True)

    @kernel
    def gate_exit(self):
        """Low-level function to exit a counting gate."""
        self.gate(False)

    @kernel
    def detect_mu(self, duration: TInt64):
        """Perform detection on this PMT channel.

        :param duration: Duration of detection in machine units
        """
        self.gate_enter()
        delay_mu(duration)
        self.gate_exit()

    @kernel
    def detect(self, duration: TFloat):
        """Perform detection on this PMT channel.

        :param duration: Duration of detection in seconds
        """
        self.detect_mu(self.core.seconds_to_mu(duration))

    @kernel
    def count(self) -> TInt32:
        """Return the PMT count.

        :return: The count of the given PMT channel
        """
        return self.edge_counter.fetch_count()

    @rpc(flags={'async'})
    def append(self, count: TInt32, archive: TBool = True):
        """Append a count to the buffers.

        This function is intended to be fast to allow high input data throughput.
        No type checking is performed on the data.
        """
        if self._first_append:
            # Clear plot to initialize plot dataset
            self.clear_counts_plot()
            # Reset flag
            self._first_append = False

        # Append to plot dataset
        self.append_to_dataset(self._count_plot_key, count)

        if archive:
            # Store count in cache
            self._count_buffer.append(count)

    @rpc(flags={'async'})
    def flush(self):
        """Flush the count buffer."""
        # Store buffer in archive
        archive_key = self.COUNT_ARCHIVE_KEY_FORMAT.format(key=self.get_name(), index=len(self._count_cache))
        self.set_dataset(archive_key, self._count_buffer, archive=True)
        # Store buffer in cache
        self._count_cache.append(self._count_buffer)
        # Create a new buffer (clearing it might result in data loss due to how the dataset manager works)
        self._count_buffer = []

    @kernel
    def store(self) -> TInt32:
        """Fetch, store, and return the PMT count."""
        count = self.count()
        self.append(count)
        return count

    @host_only
    def get_counts(self) -> typing.Sequence[typing.Sequence[int]]:
        """Return all previously recorded counts.

        :return: Sequence with count buffers (sequence of sequences)
        """
        return self._count_cache.copy()  # Return a shallow copy

    @rpc(flags={'async'})
    def plot_counts(self, **kwargs):  # type: (typing.Any) -> None
        """Plot counts.

        This function can only be called after the module is initialized.

        :param kwargs: Extra keyword arguments for the plot
        """

        # Set defaults
        kwargs.setdefault('title', f'{self.get_name().capitalize()} (RID {self._scheduler.rid})')
        for k, v in self.COUNT_PLOT_DEFAULT_KWARGS.items():
            kwargs.setdefault(k, v)
        # Plot
        self._ccb.plot_xy(self._count_plot_name, self._count_plot_key,
                          group=self.PLOT_GROUP, **kwargs)

    @rpc(flags={'async'})
    def clear_counts_plot(self):  # type: () -> None
        """Clear the counts plot.

        This function can only be called after the module is initialized.
        """
        # Set the probability dataset to an empty list
        self.set_dataset(self._count_plot_key, [], broadcast=True, archive=False)

    @rpc(flags={'async'})
    def disable_counts_plot(self):  # type: () -> None
        """Close the counts plot.

        This function can only be called after the module is initialized.
        """
        self._ccb.disable_applet(self._count_plot_name, self.PLOT_GROUP)

    @host_only
    def calibrate_offset(self, duration: float = 1 * s) -> None:
        """Calibrate the detection offset.

        This function assumes that only background noise is detected by the PMT.

        :param duration: Duration to obtain counts
        """
        assert isinstance(duration, float)
        assert duration > 0.0

        # Get count
        count = self._get_offset_count(duration)
        # Store offset
        offset = count / duration
        self.set_dataset_sys(self.DETECTION_OFFSET_KEY, offset)
        self.logger.info(f'Detection offset set to {freq_to_str(offset)}')

    @portable
    def get_offset(self) -> TInt32:
        """Get the detection offset.

        :return: Detection offset in Hz
        """
        return self._detection_offset

    @kernel
    def _get_offset_count(self, duration: TFloat) -> TInt32:
        self.core.reset()
        self.detect(duration)
        return self.count()


class PmtModule(SafetyContext, DetectionInterface):
    PMT_ARRAY_LABELS: typing.ClassVar[typing.List[str]] = ['Ca', 'K']
    """Labels that match the PMT array returned by :func:`get_pmt_array()`."""
    MAX_BUFFER_SIZE: typing.ClassVar[int] = 64
    """Maximum buffer size of the Kasli."""
    DEFAULT_BUFFER_SIZE: typing.ClassVar[int] = 4
    """Default buffer size for buffered functions."""

    assert 0 < DEFAULT_BUFFER_SIZE <= MAX_BUFFER_SIZE, 'Invalid default buffer size'

    # System dataset keys
    DETECTION_TIME_KEY = 'detection_time'
    DETECTION_WINDOW_KEY = 'detection_window'
    DETECTION_DELAY_KEY = 'detection_delay'

    def build(self) -> None:
        # Call super
        super(PmtModule, self).build(enter_cb=self.enter, exit_cb=self.exit, exit_error=True)

        # Class variable kernel invariants
        self.update_kernel_invariants('MAX_BUFFER_SIZE')

        # Create PMT channel modules
        self.ca = PmtChannelModule(self, 'ca', ttl_key='ttl0')
        self.k = PmtChannelModule(self, 'k', ttl_key='ttl1')
        self._channels = [self.ca, self.k]
        self.update_kernel_invariants('ca', 'k', '_channels')

        # Get scheduler
        self._scheduler = self.get_device('scheduler')
        self.update_kernel_invariants('_scheduler')
        # Get CCB tool
        self._ccb = get_ccb_tool(self)

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # Default detection time
        self._detection_time: float = self.get_dataset_sys(self.DETECTION_TIME_KEY, 100 * ms)
        self._detection_window: float = self.get_dataset_sys(self.DETECTION_WINDOW_KEY, 100 * ms)
        self._detection_delay: float = self.get_dataset_sys(self.DETECTION_DELAY_KEY, 0 * s)
        self.update_kernel_invariants('_detection_time', '_detection_window', '_detection_delay')

        if force:
            # Initialize devices
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # For initialization, always reset core first
        self.core.reset()

        # Set direction of PMT TTL pins
        for c in self._channels:
            c.ttl.input()
            delay_mu(np.int64(self.core.ref_multiplier))  # Added minimal delay to make the events sequential

        # Wait until all events have been submitted, always required for initialization
        self.core.wait_until_mu(now_mu())

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def gate(self, state: TBool):
        """Enter/exit a detection gate on all PMTs."""
        for c in self._channels:
            c.gate(state)

    @kernel
    def gate_enter(self):
        """Enter a detection gate on all PMTs."""
        self.gate(True)

    @kernel
    def gate_exit(self):
        """Exit a detection gate on all PMTs."""
        self.gate(False)

    @kernel
    def detect_mu(self, duration: TInt64 = 0):
        """Detect ions using all PMT channels (symmetric operation).

        :param duration: Duration of detection in machine units, default duration if none given
        """
        if duration <= 0:
            # Use default duration
            duration = self.core.seconds_to_mu(self._detection_time)

        # Perform parallel detection
        self.gate_enter()
        # Added extra int64() cast to prevent compiler error `KeyError: duration`
        delay_mu(np.int64(duration))
        self.gate_exit()

    @kernel
    def detect(self, duration: TFloat = 0.0):
        """Detect ions using all PMT channels (symmetric operation).

        Counts have to be obtained from the individual PMT channels.

        :param duration: Duration of detection, default duration if none given
        """
        self.detect_mu(self.core.seconds_to_mu(duration))

    @rpc(flags={'async'})
    def _append(self, ca_count: TInt32, k_count: TInt32, archive: TBool):
        # self.in_context() should NOT be used here due to delayed kernel-host variable synchronization
        self.ca.append(ca_count, archive=archive)
        self.k.append(k_count, archive=archive)

    @kernel
    def store(self):
        """Store a single count for all PMT channels."""
        self._append(self.ca.count(), self.k.count(), archive=self.in_context())

    @kernel
    def store_return_ca(self) -> TInt32:
        """Store a single count for all PMT channels and return the count of Ca.

        :return: The Ca count
        """
        ca_count = self.ca.count()
        self._append(ca_count, self.k.count(), archive=self.in_context())
        return ca_count

    @rpc(flags={'async'})
    def _flush(self):
        for c in self._channels:
            c.flush()

    @portable
    def enter(self):
        """Enter the PMT module context for capturing data in batches."""
        pass

    @portable
    def exit(self):
        """Exit the PMT module context."""
        self._flush()

    @kernel
    def count_sequence_mu(self, num_counts: TInt32,
                          detection_window: TInt64 = 0, detection_delay: TInt64 = 0,
                          buffer_size: TInt32 = DEFAULT_BUFFER_SIZE, setup: TBool = True, cleanup: TBool = True):
        """Do a high-performance PMT count for a given number of times.

        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param num_counts: The number of counts to perform
        :param detection_window: The detection window in machine units
        :param detection_delay: The detection delay in machine units (i.e. delay between detection windows)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        """
        # Set defaults
        if detection_window <= 0:
            detection_window = self.core.seconds_to_mu(self._detection_window)
        if detection_delay <= 0:
            detection_delay = self.core.seconds_to_mu(self._detection_delay)

        # Limit values
        detection_window = max(np.int64(self.core.ref_multiplier), detection_window)
        detection_delay = max(np.int64(self.core.ref_multiplier), detection_delay)
        buffer_size = min(min(max(0, buffer_size), num_counts), self.MAX_BUFFER_SIZE)

        if setup:
            for _ in range(buffer_size):
                # Build up a buffer
                delay_mu(detection_delay)
                self.detect_mu(detection_window)

        for _ in range(num_counts - buffer_size):
            # Body
            delay_mu(detection_delay)
            self.detect_mu(detection_window)
            self.store()

        if cleanup:
            for _ in range(buffer_size):
                # Clear out buffer
                self.store()

    @kernel
    def count_sequence(self, num_counts: TInt32,
                       detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0,
                       buffer_size: TInt32 = DEFAULT_BUFFER_SIZE, setup: TBool = True, cleanup: TBool = True):
        """Do a high-performance PMT count for a given number of times.


        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param num_counts: The number of counts to perform
        :param detection_window: The detection window in seconds
        :param detection_delay: The detection delay in seconds (i.e. delay between detection windows)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        """
        self.count_sequence_mu(num_counts=num_counts,
                               detection_window=self.core.seconds_to_mu(detection_window),
                               detection_delay=self.core.seconds_to_mu(detection_delay),
                               buffer_size=buffer_size,
                               setup=setup,
                               cleanup=cleanup)

    @kernel
    def count_window_mu(self, duration: TInt64, detection_window: TInt64 = 0, detection_delay: TInt64 = 0,
                        buffer_size: TInt32 = DEFAULT_BUFFER_SIZE,
                        setup: TBool = True, cleanup: TBool = True) -> TInt32:
        """Do a high-performance PMT count for a given amount of time.


        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param duration: The total detection time in machine units (number of windows will be **round up**)
        :param detection_window: The detection window in machine units
        :param detection_delay: The detection delay in machine units (i.e. delay between detection windows)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        :return: Number of counts performed
        """
        # Set defaults
        if detection_window <= 0:
            detection_window = self.core.seconds_to_mu(self._detection_window)
        if detection_delay <= 0:
            detection_delay = self.core.seconds_to_mu(self._detection_delay)

        # Limit values
        detection_window = max(np.int64(self.core.ref_multiplier), detection_window)
        detection_delay = max(np.int64(self.core.ref_multiplier), detection_delay)

        # Convert time to number of counts
        num_counts = np.int32(duration // (detection_window + detection_delay))
        if duration % (detection_window + detection_delay):
            num_counts += 1  # Round up

        self.count_sequence_mu(num_counts=num_counts,
                               detection_window=detection_window,
                               detection_delay=detection_delay,
                               buffer_size=buffer_size,
                               setup=setup,
                               cleanup=cleanup)
        return num_counts

    @kernel
    def count_window(self, duration: TFloat, detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0,
                     buffer_size: TInt32 = DEFAULT_BUFFER_SIZE, setup: TBool = True, cleanup: TBool = True) -> TInt32:
        """Do a high-performance PMT count for a given amount of time.


        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param duration: The total detection time in seconds (number of windows will be **round up**)
        :param detection_window: The detection window in seconds
        :param detection_delay: The detection delay in seconds (i.e. delay between detection windows)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        :return: Number of counts performed
        """
        return self.count_window_mu(duration=self.core.seconds_to_mu(duration),
                                    detection_window=self.core.seconds_to_mu(detection_window),
                                    detection_delay=self.core.seconds_to_mu(detection_delay),
                                    buffer_size=buffer_size,
                                    setup=setup,
                                    cleanup=cleanup)

    @kernel
    def count_threshold_mu(self, threshold: TInt32,
                           detection_window: TInt64 = 0, detection_delay: TInt64 = 0,
                           buffer_size: TInt32 = DEFAULT_BUFFER_SIZE,
                           setup: TBool = True, cleanup: TBool = True) -> TTuple([TInt32, TInt32]):
        """Do a high-performance PMT count until a threshold is reached on Ca.


        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param threshold: The threshold for Ca detection
        :param detection_window: The detection window in machine units
        :param detection_delay: The detection delay in machine units (i.e. delay between detection windows)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        :return: (Number of counts performed, last Ca count)
        """
        # Set defaults
        if detection_window <= 0:
            detection_window = self.core.seconds_to_mu(self._detection_window)
        if detection_delay <= 0:
            detection_delay = self.core.seconds_to_mu(self._detection_delay)

        # Limit values
        detection_window = max(np.int64(self.core.ref_multiplier), detection_window)
        detection_delay = max(np.int64(self.core.ref_multiplier), detection_delay)
        buffer_size = min(max(0, buffer_size), self.MAX_BUFFER_SIZE)

        # Counter
        counter = buffer_size
        # Initialize Ca count (mitigates ARTIQ compiler issue)
        # noinspection PyUnusedLocal
        ca_count = 0

        if setup:
            for _ in range(buffer_size):
                # Build up a buffer
                delay_mu(detection_delay)
                self.detect_mu(detection_window)

        while True:
            # Body
            delay_mu(detection_delay)
            self.detect_mu(detection_window)
            counter += 1
            ca_count = self.store_return_ca()

            # Break conditions
            if ca_count > threshold or self._scheduler.check_pause():
                break

        if cleanup:
            for _ in range(buffer_size):
                # Clear out buffer
                self.store()

        # Return number of counts performed and the last Ca count
        return counter, ca_count

    @kernel
    def count_threshold(self, threshold: TInt32,
                        detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0,
                        buffer_size: TInt32 = DEFAULT_BUFFER_SIZE,
                        setup: TBool = True, cleanup: TBool = True) -> TTuple([TInt32, TInt32]):
        """Do a high-performance PMT count until a threshold is reached on Ca.


        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param threshold: The threshold for Ca detection
        :param detection_window: The detection window in seconds
        :param detection_delay: The detection delay in seconds (i.e. delay between detection windows)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        :return: (Number of counts performed, last Ca count)
        """
        return self.count_threshold_mu(threshold=threshold,
                                       detection_window=self.core.seconds_to_mu(detection_window),
                                       detection_delay=self.core.seconds_to_mu(detection_delay),
                                       buffer_size=buffer_size,
                                       setup=setup,
                                       cleanup=cleanup)

    @rpc(flags={'async'})
    def plot_counts(self, **kwargs):  # type: (typing.Any) -> None
        """Plot Ca and K counts.

        This function can only be called after the module is initialized.

        :param kwargs: Extra keyword arguments for the plots (arguments forwarded to all plots)
        """
        for c in self._channels:
            c.plot_counts(**kwargs)

    @rpc(flags={'async'})
    def clear_counts_plot(self):  # type: () -> None
        """Clear the Ca and K counts plot.

        This function can only be called after the module is initialized.
        """
        for c in self._channels:
            c.clear_counts_plot()

    @rpc(flags={'async'})
    def disable_counts_plot(self):  # type: () -> None
        """Disable the Ca and K counts plot.

        This function can only be called after the module is initialized.
        """
        for c in self._channels:
            c.disable_counts_plot()

    @rpc(flags={'async'})
    def disable_plots(self):  # type: () -> None
        """Close all plots.

        This function can only be called after the module is initialized.
        """
        self._ccb.disable_applet_group(PmtChannelModule.PLOT_GROUP)

    """Interface functions"""

    @host_only
    def get_pmt_array(self) -> typing.List[artiq.coredevice.edge_counter.EdgeCounter]:
        return [c.edge_counter for c in self._channels]

    @host_only
    def get_state_detection_threshold(self) -> int:
        return 0  # Return a number to satisfy the histogram context

    @host_only
    def get_default_detection_time(self) -> float:
        return self._detection_time
