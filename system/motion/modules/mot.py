import typing

from dax.experiment import *
from dax.modules.safety_context import SafetyContext, ReentrantSafetyContext
from dax.util.units import volt_to_str, ampere_to_str


class PsuModule(SafetyContext):
    """Generic PSU module."""

    def build(self, *,
              key: str,
              target_output: str,
              **kwargs: typing.Any) -> None:
        assert isinstance(key, str)
        assert isinstance(target_output, str)

        # Store attributes
        self._target_output: str = target_output
        # Get devices
        self._psu = self.get_device(key)
        # Call super
        super(PsuModule, self).build(enter_cb=self.on, exit_cb=self.off, rpc_=True, **kwargs)

    """Module functionality"""

    @rpc
    def config(self, voltage: TFloat, current: TFloat = None):
        assert isinstance(voltage, float), 'Voltage must be a float'
        assert isinstance(current, float) or current is None, 'Current must be a float or None'
        self.logger.debug(f'Setting target output {self._target_output} to {volt_to_str(voltage)}'
                          f'{"" if current is None else f", {ampere_to_str(current)}"}')
        self._psu.set_output_target(self._target_output)
        self._psu.apply(self._target_output, voltage, current)

    @rpc
    def get_output_settings(self) -> TTuple([TFloat, TFloat]):
        """Return the voltage and current values of the power supply."""
        voltage, current = self._power_supply.get_output_settings(self._target_output)
        self.logger.debug(f'Output settings: {volt_to_str(voltage)}, {ampere_to_str(current)}')
        return voltage, current

    @rpc
    def set(self, state: TBool):
        self._psu.enable_outputs(state)

    @rpc
    def on(self):
        self.set(True)

    @rpc
    def off(self):
        self.set(False)


class DefaultsPsuModule(PsuModule):
    """Generic PSU module with a default configuration."""

    # System dataset keys
    DEFAULT_VOLTAGE_KEY = 'voltage'
    DEFAULT_CURRENT_KEY = 'current'

    def build(self, *,
              default_voltage: float,
              default_current: float,
              init_reset: bool = True,
              **kwargs: typing.Any) -> None:
        assert isinstance(default_voltage, float)
        assert isinstance(default_current, float)
        assert isinstance(init_reset, bool)

        # Store attributes
        self._default_voltage: float = default_voltage
        self._default_current: float = default_current
        self._init_reset: bool = init_reset
        # Call super
        super(DefaultsPsuModule, self).build(**kwargs)

    @host_only
    def init(self) -> None:
        # Call super
        super(DefaultsPsuModule, self).init()

        # Get system datasets
        self._voltage: float = self.get_dataset_sys(self.DEFAULT_VOLTAGE_KEY, self._default_voltage)
        self._current: float = self.get_dataset_sys(self.DEFAULT_CURRENT_KEY, self._default_current)

        if self._init_reset:
            # Reset configuration
            self.reset()

    """Module functionality"""

    @rpc
    def reset(self):
        self.config(self._voltage, self._current)


class MotModule(ReentrantSafetyContext):

    def build(self) -> None:
        # Submodules for devices
        self._helmholtz = DefaultsPsuModule(self, 'helmholtz', key='helmholtz_psu', target_output='P6V',
                                            default_voltage=5 * V, default_current=2 * A)
        self._psu1 = PsuModule(self, 'psu1', key='power_supply_1', target_output='P50V')
        self._psu2 = DefaultsPsuModule(self, 'psu2', key='power_supply_2', target_output='P50V',
                                       default_voltage=30 * V, default_current=2 * A)
        self.update_kernel_invariants('_helmholtz', '_psu1', '_psu2')

        # Call super
        super(MotModule, self).build(rpc_=True, enter_cb=self.__enter_safety, exit_cb=self.exit_safety)

    @host_only
    def init(self) -> None:
        pass

    @host_only
    def post_init(self) -> None:
        pass

    """Module functionality"""

    @portable
    def __enter_safety(self):
        pass

    @rpc
    def exit_safety(self):
        """Switch off all PSU's"""
        for psu in [self._helmholtz, self._psu1, self._psu2]:
            if psu.in_context:
                psu.__exit__(None, None, None)
            else:
                psu.off()  # Also switch off if we are not in context

    @rpc
    def _ion_ca_mf_on(self):
        """Switch on ion magnetic field."""
        self._psu1.config(30 * V, 0.7 * A)
        if not self._psu1.in_context():
            self._psu1.__enter__()

    @rpc
    def ion_ca_mf_on(self):
        """Switch on the ion magnetic field for ion loading."""

        if self.in_context():
            raise self.EXCEPTION_TYPE('This function can only be used out of context')

        # Switch on ion magnetic field
        self._ion_ca_mf_on()

    @rpc
    def _antihelmholtz_on(self):
        """Switch on PSU1 and PSU2, antihelmholtz config"""
        self._psu1.config(40 * V, 3.1 * A)
        self._psu2.config(40 * V, 3.0 * A)
        for psu in [self._psu1, self._psu2]:
            if not psu.in_context():
                psu.__enter__()

    @rpc
    def antihelm_pmtswitch_on(self):
        """Switch on PSU 1 and PSU 2 (antihelmholtz config) outside the safety context"""

        if self.in_context():
            raise self.EXCEPTION_TYPE('This function can only be used out of context')

        # switch on antihelmholtz config
        self._antihelmholtz_on()

    @rpc
    def ion_mf_on(self):
        """Switch on the ion magnetic field (and switch off MOT magnetic field)."""

        if not self.in_context():
            raise self.EXCEPTION_TYPE('This function can only be used in context')

        # Switch off other PSU's
        self._psu2.__exit__(None, None, None)
        self._helmholtz.__exit__(None, None, None)

        # Switch on ion magnetic field
        self._ion_ca_mf_on()

    @rpc
    def antihelmholtz_on(self):
        """Switch on PSU1 and 2 (antihelholtz config) inside the safety context"""

        if not self.in_context():
            raise self.EXCEPTION_TYPE("This function can only be used in context")

        # Switch on antihelmholtz config
        self._antihelmholtz_on()

    @rpc
    def mot_helmholtz_on(self):
        """Switch on MOT magnetic field and helmholtz."""

        if not self.in_context():
            raise self.EXCEPTION_TYPE('This function can only be used in context')

        # Switch on MOT magnetic field
        for psu in [self._helmholtz]:
            if not psu.in_context():
                psu.__enter__()

    @rpc
    def helmholtz_off(self):
        """Switch off helmholtz."""

        if not self.in_context():
            raise self.EXCEPTION_TYPE('This function can only be used in context')

        # Switch off helmholtz
        self._helmholtz.__exit__(None, None, None)

    @rpc
    def mot_off(self):
        """Switch off MOT magnetic field."""

        if not self.in_context():
            raise self.EXCEPTION_TYPE('This function can only be used in context')

        # MOT magnetic field off
        self._psu1.__exit__(None, None, None)
        self._psu2.__exit__(None, None, None)
