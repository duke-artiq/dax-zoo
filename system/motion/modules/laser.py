import typing
import abc
import numpy as np

import artiq.coredevice.ad9910

from dax.experiment import *
from dax.util.units import time_to_str, freq_to_str
from dax.modules.safety_context import SafetyContext


class BaseLaserModule(DaxModule, abc.ABC):
    # System dataset keys
    DDS_ATT_KEY = 'dds_att'
    DDS_LATENCY_MU_KEY = 'dds_latency_mu'

    # Default attenuation
    DEFAULT_DDS_ATT = 0 * dB

    # noinspection PyMethodOverriding
    def build(self, *, dds_key: str) -> None:
        # Get devices
        self._dds = self.get_device(dds_key, artiq.coredevice.ad9910.AD9910)
        self.update_kernel_invariants('_dds')

        # DDS att range check
        assert 0.0 <= self.DEFAULT_DDS_ATT <= 31.5 * dB, 'DDS attenuation out of range'

    @host_only
    def init(self, *, force: bool = False) -> None:
        # System datasets
        self._dds_att: float = self.get_dataset_sys(self.DDS_ATT_KEY, self.DEFAULT_DDS_ATT)
        self._dds_latency_mu: np.int64 = self.get_dataset_sys(self.DDS_LATENCY_MU_KEY, np.int64(0))
        self.update_kernel_invariants('_dds_att', '_dds_latency_mu')

        if force:
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # Reset core
        self.core.reset()

        # Set switch
        self.off()
        # Reset attenuation
        self.reset_att()
        # Reset DDS
        self.reset(realtime=False)

        # Sync
        self.core.wait_until_mu(now_mu())

    @host_only
    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def _dds_config_mu(self, ftw: TInt32, asf: TInt32):
        """Private function to configure the DDS, used for overriding behavior of Ca laser.

        :param ftw: Frequency tuning word
        :param asf: Amplitude scaling factor
        """
        # Configure DDS
        self._dds.set_mu(ftw, asf=asf)

    @kernel
    def config_mu(self, ftw: TInt32, asf: TInt32, realtime: TBool = False):
        """Configure the laser.

        :param ftw: Frequency tuning word
        :param asf: Amplitude scaling factor
        :param realtime: :const:`True` to compensate for programming latencies
        """
        if realtime:
            # Compensate for DDS latency
            delay_mu(np.int64(-self._dds_latency_mu))
        else:
            # Add some slack
            delay(200 * us)

        self._dds_config_mu(ftw, asf)

    @kernel
    def config(self, freq: TFloat, amp: TFloat, realtime: TBool = False):
        """Configure the laser.

        :param freq: Frequency
        :param amp: Amplitude ``[0.0, 1.0]``
        :param realtime: :const:`True` to compensate for programming latencies
        """
        self.config_mu(self.freq_to_ftw(freq), self.amp_to_asf(amp), realtime=realtime)

    @abc.abstractmethod
    def reset(self, realtime: TBool = False):
        pass

    @kernel
    def config_att(self, att: TFloat):
        """Configure the attenuation.

        :param att: Attenuation in dB
        """
        # Add some slack
        delay(200 * us)

        # Configure att
        self._dds.set_att(att)

    @kernel
    def reset_att(self):
        """Reset the attenuation."""
        self.config_att(self._dds_att)

    @kernel
    def set(self, state: TBool):
        """Set the state.

        :param state: `True` to enable this laser
        """
        self._dds.sw.set_o(state)

    @kernel
    def on(self):
        """Enable laser."""
        self.set(True)

    @kernel
    def off(self):
        """Disable laser."""
        self.set(False)

    @kernel
    def pulse_mu(self, duration: TInt64):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in machine units
        """
        try:
            self.on()
            delay_mu(duration)
            self.off()
        except RTIOUnderflow:
            self.safety_off()
            self.core.wait_until_mu(now_mu())
            raise

    @kernel
    def pulse(self, duration: TFloat):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in seconds
        """
        self.pulse_mu(self.core.seconds_to_mu(duration))

    @portable
    def freq_to_ftw(self, freq: TFloat) -> TInt32:
        return self._dds.frequency_to_ftw(freq)

    @portable
    def amp_to_asf(self, amp: TFloat) -> TInt32:
        return self._dds.amplitude_to_asf(amp)

    @kernel
    def safety_off(self):
        """Switch laser off in a safe manner.

        This function is slow and should not raise any exceptions.
        """
        self.core.break_realtime()
        self.off()
        self.core.wait_until_mu(now_mu())

    @host_only
    def update_latency(self) -> None:
        """Update the DDS latency.

        Note: **this operation will reset the DDS to its default configuration!**
        """

        # Reset the latency to zero
        self._dds_latency_mu = np.int64(0)
        # Obtain current latency
        latency_mu = self._get_latency()
        # Store latency
        self.logger.info(f'Obtained latency: {latency_mu} machine units '
                         f'({time_to_str(self.core.mu_to_seconds(latency_mu))})')
        self.set_dataset_sys(self.DDS_LATENCY_MU_KEY, latency_mu)

    @kernel
    def _get_latency(self) -> TInt64:
        # Reset core
        self.core.reset()

        # Switch off laser
        self.off()
        # Reset in real-time and capture start and end time
        delay(1 * ms)
        t_start = now_mu()
        self.reset(realtime=True)
        t_end = now_mu()
        self.core.wait_until_mu(t_end)

        # Calculate and return latency
        return t_end - t_start

    @host_only
    def save_att(self, att: float) -> None:
        """Save a new default attenuation."""
        assert isinstance(att, float), 'Attenuation must be of type float'
        assert 0 * dB <= att <= 31 * dB, 'Attenuation out of range'
        self.set_dataset_sys(self.DDS_ATT_KEY, att)


class CaLaserModule(BaseLaserModule):
    # System dataset keys
    AMP_SCALE_FACTOR_KEY = 'amp_scale_factor'
    RESONANCE_FREQ_KEY = 'resonance_freq'
    LOAD_CA_DETUNING_KEY = 'load_ca_detuning'
    LOAD_CA_AMP_KEY = 'load_ca_amp'
    LOAD_CAH_DETUNING_KEY = 'load_cah_detuning'
    LOAD_CAH_AMP_KEY = 'load_cah_amp'
    HOLD_CA_DETUNING_KEY = 'hold_ca_detuning'
    HOLD_CA_AMP_KEY = 'hold_ca_amp'
    COOL_CA_DETUNING_KEY = 'cool_ca_detuning'
    COOL_CA_AMP_KEY = 'cool_ca_amp'
    TARGET_PMT_KEY = 'target_pmt_count'

    # Default DDS attenuation for Ca laser
    DEFAULT_DDS_ATT = 31.5 * dB

    @host_only
    def init(self) -> None:
        # Call super
        super(CaLaserModule, self).init()

        # Load system datasets
        self._amp_scale_factor: float = self.get_dataset_sys(self.AMP_SCALE_FACTOR_KEY, 0.21)
        self._resonance_freq: float = self.get_dataset_sys(self.RESONANCE_FREQ_KEY, 90 * MHz)
        self._load_ca_detuning: float = self.get_dataset_sys(self.LOAD_CA_DETUNING_KEY, 30 * MHz)
        self._load_ca_amp: float = self.get_dataset_sys(self.LOAD_CA_AMP_KEY, 0.59)
        self._load_cah_detuning: float = self.get_dataset_sys(self.LOAD_CAH_DETUNING_KEY, 30 * MHz)
        self._load_cah_amp: float = self.get_dataset_sys(self.LOAD_CAH_AMP_KEY, 0.45)
        self._hold_ca_detuning: float = self.get_dataset_sys(self.HOLD_CA_DETUNING_KEY, 60 * MHz)
        self._hold_ca_amp: float = self.get_dataset_sys(self.HOLD_CA_AMP_KEY, 0.50)
        self._cool_ca_detuning: float = self.get_dataset_sys(self.COOL_CA_DETUNING_KEY, 10 * MHz)
        self._cool_ca_amp: float = self.get_dataset_sys(self.COOL_CA_AMP_KEY, 0.4)
        self.update_kernel_invariants('_amp_scale_factor', '_resonance_freq',
                                      '_load_ca_detuning', '_load_ca_amp',
                                      '_load_cah_detuning', '_load_cah_amp',
                                      '_hold_ca_detuning', '_hold_ca_amp',
                                      '_cool_ca_detuning', '_cool_ca_amp')

    """Module functionality"""

    @kernel(flags={'fast_math'})
    def _dds_config_mu(self, ftw: TInt32, asf: TInt32):
        # Scale amplitude and set DDS
        self._dds.set_mu(ftw, asf=np.int32(asf * self._amp_scale_factor))

    @kernel
    def reset(self, realtime: TBool = False):
        """Reset the configuration of this laser by setting it to the load Ca profile."""
        self.load_ca(realtime=realtime)

    @portable(flags={'fast-math'})
    def _detuning_to_freq(self, detuning: TFloat) -> TFloat:
        """Convert a detuning to an absolute frequency."""
        return self._resonance_freq + detuning / 2

    @kernel
    def detune(self, detuning: TFloat, amp: TFloat, realtime: TBool = False) -> TFloat:
        """Configure this laser based on a detuning from resonance frequency.

        :param detuning: Detuning from resonance frequency
        :param amp: Amplitude ``[0.0, 1.0]``
        :param realtime: :const:`True` to compensate for programming latencies
        :return: The configured frequency
        """
        freq = self._detuning_to_freq(detuning)
        self.config(freq, amp=amp, realtime=realtime)
        return freq

    @kernel
    def load_ca(self, realtime: TBool = False):
        """Set the load Ca profile."""
        self.config(self._detuning_to_freq(self._load_ca_detuning),
                    amp=self._load_ca_amp, realtime=realtime)

    @kernel
    def load_cah(self, realtime: TBool = False):
        """Set the load CaH profile."""
        self.config(self._detuning_to_freq(self._load_cah_detuning),
                    amp=self._load_cah_amp, realtime=realtime)

    @kernel
    def hold_ca(self, realtime: TBool = False):
        """Set the hold Ca profile."""
        self.config(self._detuning_to_freq(self._hold_ca_detuning),
                    amp=self._hold_ca_amp, realtime=realtime)

    @kernel
    def cool_ca(self, realtime: TBool = False):
        """Set the cool Ca profile."""
        self.config(self._detuning_to_freq(self._cool_ca_detuning),
                    amp=self._cool_ca_amp, realtime=realtime)

    @host_only
    def get_target_pmt_freq(self) -> float:
        """Get the target pmt frequency.

        This function is safe to call at any time.
        """
        return self.get_dataset_sys(self.TARGET_PMT_KEY)

    @host_only
    def get_dds_att(self, *, fallback: float = DEFAULT_DDS_ATT) -> float:
        """Get the Ca default dds attenuation.

        This function is safe to call at any time.
        """
        assert isinstance(fallback, float)
        return self.get_dataset_sys(self.DDS_ATT_KEY, fallback=fallback)

    @host_only
    def get_resonance_freq(self, *, fallback: float = 90 * MHz) -> float:
        """Get the Ca resonance frequency.

        This function is safe to call at any time.
        """
        assert isinstance(fallback, float)
        return self.get_dataset_sys(self.RESONANCE_FREQ_KEY, fallback=fallback)

    @host_only
    def save_pmt_target_freq(self, pmt_freq: float):
        """
        Save the target pmt count frequency for calibrating power

        :param pmt_freq: the target pmt frequency
        """
        assert isinstance(pmt_freq, float), 'Target frequency must be of type float'
        assert 0.0 <= pmt_freq, 'Target frequency out of range'
        self.set_dataset_sys(self.TARGET_PMT_KEY, pmt_freq)

    @host_only
    def save_amp_scale_factor(self, asf: float) -> None:
        """Save the amplitude scaling factor.

        :param asf: The new amplitude scaling factor (0.0,1.0]
        """
        assert isinstance(asf, float), 'Amplitude scaling factor must be of type float'
        assert 0.0 < asf <= 1.0, 'Amplitude scaling factor out of range'
        self.logger.info(f'Amplitude scaling factor set to {asf}')
        self.set_dataset_sys(self.AMP_SCALE_FACTOR_KEY, asf)

    @host_only
    def save_resonance_freq(self, freq: float) -> None:
        """Save the resonance frequency.

        :param freq: The new frequency in Hz
        """
        assert isinstance(freq, float), 'Resonance frequency must be of type float'
        assert 0.0 < freq < 400 * MHz, 'Resonance frequency out of range'
        self.logger.info(f'Resonance frequency set to {freq_to_str(freq)}')
        self.set_dataset_sys(self.RESONANCE_FREQ_KEY, freq)

    @host_only
    def _save_profile(self, detuning_key: str, detuning: float, amp_key: str, amp: typing.Optional[float]) -> None:
        assert isinstance(detuning, float), 'Detuning must be of type float'
        assert 0.0 < self._resonance_freq + detuning < 400 * MHz, 'Detuned frequency out of range'
        self.logger.info(f'Detuning {detuning_key} set to {freq_to_str(detuning)}')
        self.set_dataset_sys(detuning_key, detuning)

        if amp is not None:
            assert isinstance(amp, float), 'Amplitude must be of type float'
            assert 0.0 <= amp <= 1.0, 'Amplitude out of range'
            self.logger.info(f'Amplitude {amp_key} set to {amp}')
            self.set_dataset_sys(amp_key, amp)

    @host_only
    def save_load_ca_profile(self, detuning: float, amp: typing.Optional[float] = None) -> None:
        """Save the load Ca profile.

        :param detuning: The detuning for this profile in Hz
        :param amp: (Optional) amplitude for this profile as scaling factor
        """
        self._save_profile(self.LOAD_CA_DETUNING_KEY, detuning, self.LOAD_CA_AMP_KEY, amp)

    @host_only
    def save_load_cah_profile(self, detuning: float, amp: typing.Optional[float] = None) -> None:
        """Save the load CaH profile.

        :param detuning: The detuning for this profile in Hz
        :param amp: (Optional) amplitude for this profile as scaling factor
        """
        self._save_profile(self.LOAD_CAH_DETUNING_KEY, detuning, self.LOAD_CAH_AMP_KEY, amp)

    @host_only
    def save_hold_ca_profile(self, detuning: float, amp: typing.Optional[float] = None) -> None:
        """Save the hold Ca profile.

        :param detuning: The detuning for this profile in Hz
        :param amp: (Optional) amplitude for this profile as scaling factor
        """
        self._save_profile(self.HOLD_CA_DETUNING_KEY, detuning, self.HOLD_CA_AMP_KEY, amp)

    @host_only
    def save_cool_ca_profile(self, detuning: float, amp: typing.Optional[float] = None) -> None:
        """Save the cool Ca profile.

        :param detuning: The detuning for this profile in Hz
        :param amp: (Optional) amplitude for this profile as scaling factor
        """
        self._save_profile(self.COOL_CA_DETUNING_KEY, detuning, self.COOL_CA_AMP_KEY, amp)


class KLaserModule(BaseLaserModule):
    # System dataset keys
    FREQ_KEY = 'freq'
    AMP_KEY = 'amp'

    # noinspection PyMethodOverriding
    def build(self, *, default_freq: float, default_amp: float, **kwargs: typing.Any) -> None:
        # Call super
        super(KLaserModule, self).build(**kwargs)

        # Store default values
        self._default_freq = default_freq
        self._default_amp = default_amp

    @host_only
    def init(self) -> None:
        # Call super
        super(KLaserModule, self).init()

        # Load system datasets
        self._freq: float = self.get_dataset_sys(self.FREQ_KEY, self._default_freq)
        self._amp: float = self.get_dataset_sys(self.AMP_KEY, self._default_amp)
        self.update_kernel_invariants('_freq', '_amp')

    """Module functionality"""

    @kernel
    def reset(self, realtime: TBool = False):
        """Reset the configuration of this laser to its defaults."""
        self.config_mu(self.freq_to_ftw(self._freq),
                       asf=self.amp_to_asf(self._amp),
                       realtime=realtime)

    @host_only
    def save_default_profile(self, freq: float, amp: typing.Optional[float] = None) -> None:
        """Save the default profile (loaded during DAX init).

        :param freq: The frequency for this profile in Hz
        :param amp: (Optional) amplitude for this profile as scaling factor
        """
        assert isinstance(freq, float), 'Frequency must be of type float'
        assert 0.0 < freq < 400 * MHz, 'Frequency out of range'
        self.logger.info(f'Default frequency set to {freq_to_str(freq)}')
        self.set_dataset_sys(self.FREQ_KEY, freq)

        if amp is not None:
            assert isinstance(amp, float), 'Amplitude must be of type float'
            assert 0.0 <= amp <= 1.0, 'Amplitude out of range'
            self.logger.info(f'Default amplitude set to {amp}')
            self.set_dataset_sys(self.AMP_KEY, amp)


class LaserShutterModule(SafetyContext):
    """Module to control the 767 (MOT) laser shutter."""

    # System dataset keys
    SHUTTER_LATENCY_KEY = 'shutter_latency'

    def build(self) -> None:
        # call build function of SafetyContext to use shutter as context
        super(LaserShutterModule, self).build(enter_cb=self.open, exit_cb=self.close)

        # set up shutter output (control of opening and closing the shutter)
        self._ttl = self.get_device('ttl5', artiq.coredevice.ttl.TTLOut)
        self.update_kernel_invariants('_ttl')

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # Shutter latency
        self._shutter_latency: float = self.get_dataset_sys(self.SHUTTER_LATENCY_KEY, 0.0)
        self.update_kernel_invariants('_shutter_latency')

        if force:
            # Initialize devices
            self.init_kernel()

    def post_init(self) -> None:
        pass

    @kernel
    def init_kernel(self):
        """Kernel function to initialize this module.

        This function is called automatically during initialization unless the user configured otherwise.
        In that case, this function has to be called manually.
        """
        # Reset the core
        self.core.reset()

        # Close shutter
        self.close()

        # Wait until event is submitted
        self.core.wait_until_mu(now_mu())

    """Module functionality"""

    @kernel
    def _set(self, state: TBool, wait: TBool = True):
        """Set the 767 beam state.

        :param state: The state to which set the 767 (MOT) shutter
        :param wait: Specifies whether the program waits for the shutter operation to be completed
        """
        self._ttl.set_o(state)
        if wait:
            delay(self._shutter_latency)

    @kernel
    def open(self, wait: TBool = True):
        """Open the 767 beam shutter."""
        self._set(True, wait=wait)

    @kernel
    def close(self, wait: TBool = True):
        """Close the 767 beam shutter."""
        try:
            self._set(False, wait=wait)
        except RTIOUnderflow:
            # Safety in case closing fails
            self.core.break_realtime()
            self._set(False, wait=True)
            self.core.wait_until_mu(now_mu())
            raise

    @host_only
    def save_latency(self, latency: float) -> None:
        """Save the shutter latency into dataset.

        :param latency: The latency (in seconds) to be updated
        """
        assert isinstance(latency, float), 'Latency must be of type float'
        assert latency >= 0.0, 'Latency cannot be negative'
        self.logger.info(f'Shutter latency set to {time_to_str(latency)}')
        self.set_dataset_sys(self.SHUTTER_LATENCY_KEY, latency)


class LaserModule(DaxModule):

    def build(self):
        # Get devices
        self.ca = CaLaserModule(self, 'ca', dds_key='urukul0_ch0')
        self.k1 = KLaserModule(self, 'k1', dds_key='urukul0_ch1', default_freq=118 * MHz, default_amp=0.99)
        self.k2 = KLaserModule(self, 'k2', dds_key='urukul0_ch2', default_freq=107 * MHz, default_amp=0.99)
        self.shutter767 = LaserShutterModule(self, 'laser_shutter')
        self.all_k_lasers = [self.k1, self.k2]
        self.update_kernel_invariants('ca', 'k1', 'k2', 'shutter767', 'all_k_lasers')

    def init(self, *, force: bool = False) -> None:
        if force:
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # Ca laser should not be initialized
        # Initialize K lasers
        for laser in self.all_k_lasers:
            laser.init_kernel()

        # Initialize 767 laser shutter (for MOT lasers)
        self.shutter767.init_kernel()

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def set(self, state: TBool):
        """Set the state of all lasers.

        For toggling K and Ca lasers to an inverted state, see :func:`toggle`.

        :param state: The state of all lasers
        """
        self.ca.set(state)
        for laser in self.all_k_lasers:
            laser.set(state)

    @kernel
    def off(self):
        """Disable all lasers."""
        self.set(False)

    @kernel
    def on(self):
        """Enable all lasers."""
        self.set(True)

    @kernel
    def safety_off(self):
        """Switch all laser off in a safe manner.

        This function is slow and should not raise any exceptions.
        """
        self.ca.safety_off()
        for laser in self.all_k_lasers:
            laser.safety_off()

    @kernel
    def toggle(self, state: TBool, disable_ca: TBool = False):
        """Toggle K and Ca lasers to an inverted state.

        :param state: The state of K lasers and inverted state for the Ca laser
        :param disable_ca: Override flag to disable the Ca laser (set to :const:`True` to keep Ca laser off)
        """
        self.k1.set(state)
        self.k2.set(state)
        self.ca.set(not state and not disable_ca)

    @kernel
    def toggle_gap_mu(self, state: TBool, gap: TInt64, disable_ca: TBool = False):
        """Toggle K and Ca lasers to an inverted state with a gap where all lasers are off.

         **This function does move the cursor with the time of the gap.**
         See also :func:`toggle`.

        :param state: The state of K lasers and inverted state for the Ca laser
        :param gap: The time between switching lasers off and on in machine units
        :param disable_ca: Override flag to disable the Ca laser (set to :const:`True` to keep Ca laser off)
        """
        # All lasers off
        self.off()
        # Gap
        delay_mu(gap)
        # Toggle (switches some lasers on)
        self.toggle(state, disable_ca=disable_ca)
