import typing
import numpy as np

from dax.experiment import *
from dax.util.ccb import get_ccb_tool

import artiq.coredevice.ttl

_WINDOW_T = typing.ClassVar[typing.Optional[typing.Tuple[int, int]]]


def _is_window_t(a):
    return (isinstance(a, tuple) and len(a) == 2 and a[0] < a[1]) or a is None


class TofModule(DaxModule):
    WAVEFORM_PLOT_Y_KEY_FORMAT: typing.ClassVar[str] = 'plot.{scheduler.rid}.tof.channels'
    """Dataset key format for waveform plots (channel data)."""
    WAVEFORM_PLOT_X_KEY_FORMAT: typing.ClassVar[str] = 'plot.{scheduler.rid}.tof.x'
    """Dataset key format for waveform plots (x-values)."""
    WAVEFORM_ARCHIVE_KEY_FORMAT: typing.ClassVar[str] = 'tof.{index}.{channel}'
    """Dataset key format for waveform archive."""

    PLOT_GROUP: typing.ClassVar[str] = 'tof'
    """Group for the applets."""
    WAVEFORM_PLOT_NAME: typing.ClassVar[str] = 'waveforms'
    """Applet name for waveform plot."""
    WAVEFORM_PLOT_DEFAULT_KWARGS: typing.ClassVar[typing.Dict[str, typing.Any]] = {
        'x_label': 'Time',
        'y_label': 'Voltage',
    }
    """Default keyword arguments for waveform plot."""
    WAVEFORM_PLOT_CHANNEL: typing.ClassVar[int] = 3
    """The channel of interest for real-time waveform plotting."""
    WAVEFORM_PLOT_DEFAULT_WINDOW: _WINDOW_T = (15000, 18000)
    """Default range of the waveform plot expressed in sample indices."""
    assert _is_window_t(WAVEFORM_PLOT_DEFAULT_WINDOW)

    # Scope fixed configuration
    SCOPE_DATA_TIME_INDEX: typing.ClassVar[int] = 0
    """Index of the time data in the 2-dimensional waveform datastructure."""
    SCOPE_DATA_VALUES_INDEX: typing.ClassVar[int] = 1
    """Index of the waveform data in the 2-dimensional waveform datastructure."""
    SCOPE_CHANNELS: typing.ClassVar[typing.List[int]] = [1, 2, 3, 4]
    """Ordered list of available channel numbers on the scope."""
    SCOPE_CHANNEL_LABELS: typing.ClassVar[typing.List[str]] = ['trigger', 'ch2', 'ch3', 'tof']
    """Ordered list of channel labels."""
    assert len(SCOPE_CHANNELS) == len(SCOPE_CHANNEL_LABELS)
    _SCOPE_TRIGGER_SRC: typing.ClassVar[str] = 'C1'

    # System dataset keys
    TRIGGER_PULSE_DURATION_KEY = 'trigger_pulse_duration'
    TOF_DURATION_KEY = 'tof_duration'

    def build(self) -> None:
        # Get the TTL trigger and scope controller
        self._trigger = self.get_device('ttl4', artiq.coredevice.ttl.TTLOut)
        self._scope = self.get_device('tof_scope')
        self.update_kernel_invariants('_trigger', '_scope')

        # Get scheduler
        self._scheduler = self.get_device('scheduler')
        # Get CCB tool
        self._ccb = get_ccb_tool(self)

        # Waveform cache
        self._waveform_cache: typing.List[typing.Dict[int, np.ndarray]] = []
        # Waveform plot window
        self._waveform_window: typing.Optional[_WINDOW_T] = self.WAVEFORM_PLOT_DEFAULT_WINDOW

    @host_only
    def init(self, *, force: bool = False) -> None:
        # Get system datasets
        self._trigger_pulse_duration: float = self.get_dataset_sys(self.TRIGGER_PULSE_DURATION_KEY, 1 * ms)
        self._tof_duration: float = self.get_dataset_sys(self.TOF_DURATION_KEY, 20 * us)
        self.update_kernel_invariants('_trigger_pulse_duration')

        # Create plot keys
        self._waveform_y_plot_key: str = self.WAVEFORM_PLOT_Y_KEY_FORMAT.format(scheduler=self._scheduler)
        self._waveform_x_plot_key: str = self.WAVEFORM_PLOT_X_KEY_FORMAT.format(scheduler=self._scheduler)

        if force:
            self.init_kernel()

    @kernel
    def init_kernel(self):
        # Ensure trigger is off
        self.core.reset()
        self._trigger.off()
        self.core.wait_until_mu(now_mu())

    @host_only
    def post_init(self) -> None:
        pass

    """Module functionality"""

    @rpc
    def setup(self, duration: TFloat = 0.0, **kwargs):
        """Setup TOF acquisition.

        :param duration: Acquisition duration in seconds (default duration if none is given)
        """
        assert isinstance(duration, float), 'Duration must be of type float'

        if duration <= 0 * s:
            # Use default duration
            duration = self._tof_duration

        assert duration > 0 * s, 'Duration must be greater than zero'
        self._scope.setup_acquisition(self.SCOPE_CHANNELS, duration, trigger_src=self._SCOPE_TRIGGER_SRC, **kwargs)

    @kernel
    def trigger(self):
        """Trigger TOF equipment.

        This function does **not** alter the timeline.
        """
        duration = self.core.seconds_to_mu(self._trigger_pulse_duration)
        self._trigger.pulse_mu(duration)
        delay_mu(np.int64(-duration))

    @rpc
    def store(self, timeout: TFloat = 1.5, **kwargs):
        """Read latest TOF data. Must be called before using :func:`get_waveforms`.

        Note that this is a sync RPC function.

        :param timeout: Waiting time for the scope to be triggered in seconds (read blocks until scope is triggered)
        :param kwargs: Extra keyword arguments for the scope read waveform function
        """
        # Read data
        self.logger.debug('Retrieve waveforms...')
        data = self._scope.read_waveform(self.SCOPE_CHANNELS, trigger_timeout=timeout, **kwargs)
        self.logger.debug('Waveforms received!')

        # Archive data, add friendly keys to dict, and cache data
        for c, l in zip(self.SCOPE_CHANNELS, self.SCOPE_CHANNEL_LABELS):
            self.set_dataset(self.WAVEFORM_ARCHIVE_KEY_FORMAT.format(index=len(self._waveform_cache), channel=l),
                             data[c], archive=True)
            data[l] = data[c]
        self._waveform_cache.append(data)

        # Prepare plot data
        plot_data = data[self.SCOPE_CHANNELS[self.WAVEFORM_PLOT_CHANNEL]]
        x_data = plot_data[self.SCOPE_DATA_TIME_INDEX]
        y_data = plot_data[self.SCOPE_DATA_VALUES_INDEX]
        if self._waveform_window is not None:
            # Slice data
            start, end = self._waveform_window
            x_data = x_data[start: end]
            y_data = y_data[start: end]

        # Write plot data
        self.set_dataset(self._waveform_x_plot_key, x_data, broadcast=True, archive=False)
        self.set_dataset(self._waveform_y_plot_key, y_data, broadcast=True, archive=False)

    @host_only
    def get_waveforms(self) -> typing.Sequence[typing.Dict[int, np.ndarray]]:
        """Return all previously recorded TOF waveforms. Must call :func:`store` before using this function.

        The returned datastructure is a sequence of dicts where the sequence represents the order
        of the stored waveforms. Each dict has integer keys representing the channels and values
        representing the waveform data. The integer keys can be found in :attr:`SCOPE_CHANNELS` and
        in addition there are also user-friendly string keys found in :attr:`SCOPE_CHANNEL_LABELS`.
        **We recommend using the string keys.**
        If only one of the keys is desired, filtering can be done by key type.

        Waveform data is stored in a 2-dimensional with time and values.
        The according indices for time and values are stored in :attr:`SCOPE_DATA_TIME_INDEX` and
        :attr:`SCOPE_DATA_VALUES_INDEX`, respectively.

        :return: A sequence of waveform data dicts
        """
        return self._waveform_cache.copy()

    @rpc(flags={'async'})
    def plot_waveforms(self, window=WAVEFORM_PLOT_DEFAULT_WINDOW, **kwargs):  # type: (_WINDOW_T, typing.Any) -> None
        """Plot waveforms.

        This function can only be called after the module is initialized.

        :param window: The window of the plot (x-axis)
        :param kwargs: Extra keyword arguments for the plot
        """
        assert _is_window_t(window)

        # Store the current window
        self._waveform_window = window

        # Set defaults
        kwargs.setdefault('title', f'TOF (RID {self._scheduler.rid})')
        kwargs.setdefault('x', self._waveform_x_plot_key)
        for k, v in self.WAVEFORM_PLOT_DEFAULT_KWARGS.items():
            kwargs.setdefault(k, v)
        # Plot
        self._ccb.plot_xy(self.WAVEFORM_PLOT_NAME, self._waveform_y_plot_key, group=self.PLOT_GROUP, **kwargs)

    @rpc(flags={'async'})
    def disable_waveforms_plot(self):  # type: () -> None
        """Close the waveform plot.

        This function can only be called after the module is initialized.
        """
        self._ccb.disable_applet(self.WAVEFORM_PLOT_NAME, self.PLOT_GROUP)
