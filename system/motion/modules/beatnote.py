from dax.experiment import *
from dax.util.units import freq_to_str

import artiq.coredevice.ad9910


class BeatnoteModule(DaxModule):
    _DEFAULT_FREQ = 310 * MHz
    """The default frequency to use when none is available."""

    # System datasets
    FREQ_KEY = 'freq'

    def build(self) -> None:
        # Obtain devices
        self._dds = self.get_device("urukul1_ch2", artiq.coredevice.ad9910.AD9910)
        self.update_kernel_invariants('_dds')

    @host_only
    def init(self) -> None:
        self._freq = self.get_dataset_sys(self.FREQ_KEY, self._DEFAULT_FREQ)
        self.update_kernel_invariants('_freq')

    @host_only
    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def set(self, state: TBool):
        """Set the state of the RF switch.

        :param state: `True` to enable the RF switch
        """
        self._dds.sw.set_o(state)

    @kernel
    def on(self):
        """Enable the RF switch."""
        self.set(True)

    @kernel
    def off(self):
        """Disable the RF switch."""
        self.set(False)

    @kernel
    def config_mu(self, ftw: TInt32, asf: TInt32 = 0x3fff):
        # Add slack
        delay(200 * us)
        # Set DDS
        self._dds.set_mu(ftw, asf=asf)

    @kernel
    def config(self, freq: TFloat, amp: TFloat = 1.0):
        self.config_mu(self._dds.frequency_to_ftw(freq), asf=self._dds.amplitude_to_asf(amp))

    @kernel
    def reset(self, amp: TFloat = 1.0):
        # Set attenuator
        delay(100 * us)
        self._dds.set_att(0 * dB)

        # Configure DDS
        self.config(self._freq, amp=amp)

    @host_only
    def save_freq(self, freq: float) -> None:
        assert isinstance(freq, float)
        assert 1 * MHz <= freq <= 400 * MHz, 'Frequency out of range'
        self.logger.info(f'Updating frequency to {freq_to_str(freq)}')
        self.set_dataset_sys(self.FREQ_KEY, freq)

    @host_only
    def get_freq(self) -> float:
        """Get the beatnote frequency or the fallback value.

        Safe to be called in ``build()``.
        """
        return self.get_dataset_sys(self.FREQ_KEY, fallback=self._DEFAULT_FREQ)
