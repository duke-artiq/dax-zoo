import artiq.coredevice.ttl  # type: ignore
from dax.util.units import time_to_str
from dax.experiment import *
from dax.modules.safety_context import SafetyContext


class IonizingShutterModule(SafetyContext):
    """Module to control the ionizing shutter."""

    # System dataset keys
    SHUTTER_LATENCY_KEY = 'shutter_latency'

    def build(self) -> None:
        # call build function of SafetyContext to use shutter as context
        super(IonizingShutterModule, self).build(enter_cb=self._open, exit_cb=self._close)

        # set up shutter output (control of opening and closing the shutter)
        self._ttl = self.get_device('ttl6', artiq.coredevice.ttl.TTLOut)
        self.update_kernel_invariants('_ttl')

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # Shutter latency
        self._shutter_latency: float = self.get_dataset_sys(self.SHUTTER_LATENCY_KEY, 0.0)
        self.update_kernel_invariants('_shutter_latency')

        if force:
            # Initialize devices
            self.init_kernel()

    def post_init(self) -> None:
        pass

    @kernel
    def init_kernel(self):
        """Kernel function to initialize this module.

        This function is called automatically during initialization unless the user configured otherwise.
        In that case, this function has to be called manually.
        """
        # Reset the core
        self.core.reset()

        # Add extra delay for shutter latency
        delay(self._shutter_latency)
        # Close shutter
        self._close()

        # Wait until event is submitted
        self.core.wait_until_mu(now_mu())

    """Module functionality"""

    @kernel
    def _set(self, state: TBool, wait: TBool = True):
        """Set the ionizing beam state.

        :param state: The state to which set the ionizing beam shutter
        :param wait: Specifies whether the program waits for the shutter operation to be completed
        """
        self._ttl.set_o(state)
        if wait:
            delay(self._shutter_latency)

    @kernel
    def _open(self, wait: TBool = True):
        """Open the ionizing beam shutter."""
        self._set(True, wait=wait)

    @kernel
    def _close(self, wait: TBool = True):
        """Close the ionizing beam shutter."""
        try:
            self._set(False, wait=wait)
        except RTIOUnderflow:
            # Safety in case closing fails
            self.core.break_realtime()
            self._set(False)
            self.core.wait_until_mu(now_mu())
            raise

    @kernel
    def pulse_mu(self, duration: TInt64, wait: TBool = True):
        """Pulse the output high for the specified duration.

        :param duration: Amount of time for shutter to be open in machine units
        :param wait: Specifies whether the program waits for the shutter operation to be completed
        """

        assert duration >= self.core.seconds_to_mu(1 * us), 'Short pulse can potentially cause underflow exceptions'
        assert duration > self._shutter_latency, 'Pulse shorter than shutter latency'
        self._ttl.pulse_mu(duration)
        if wait:
            delay(self._shutter_latency)

    @kernel
    def pulse(self, duration: TFloat, wait: TBool = True):
        """Pulse the ionizing beam shutter high for specified duration.

        :param duration: Amount of time for shutter to be open in seconds
        :param wait: Specifies whether the program waits for the shutter operation to be completed
        """
        self.pulse_mu(self.core.seconds_to_mu(duration), wait=wait)

    @host_only
    def save_latency(self, latency: float) -> None:
        """Save the shutter latency into dataset.

        :param latency: The latency (in seconds) to be updated
        """
        assert isinstance(latency, float), 'Latency must be of type float'
        assert latency >= 0.0, 'Latency cannot be negative'
        self.logger.info(f'Shutter latency set to {time_to_str(latency)}')
        self.set_dataset_sys(self.SHUTTER_LATENCY_KEY, latency)
