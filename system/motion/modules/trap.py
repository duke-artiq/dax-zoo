import typing

from dax.experiment import *

import artiq.coredevice.zotino


class TrapDcModule(DaxModule):
    NUM_CHANNELS: int = 4
    """Number of channels for the trap DC."""
    MIN_VOLTAGE: float = 0 * V
    """Minimal allowed voltage."""
    MAX_VOLTAGE: float = 5 * V
    """Maximum allowed voltage."""

    # System dataset keys
    VOLTAGES_KEY = 'voltage'

    def build(self):
        # Class variable kernel invariants
        self.update_kernel_invariants('NUM_CHANNELS', 'MIN_VOLTAGE', 'MAX_VOLTAGE')

        # Get devices
        self._dac = self.get_device('zotino0', artiq.coredevice.zotino.Zotino)
        self.update_kernel_invariants('_dac')

    def init(self) -> None:
        # Get channel voltages
        self._voltages: typing.List[float] = self.get_dataset_sys(self.VOLTAGES_KEY, [0 * V] * self.NUM_CHANNELS)
        self.update_kernel_invariants('_voltages')

        if len(self._voltages) != self.NUM_CHANNELS:
            self.logger.warning('Number of saved voltages does not match the number of channels')

    def post_init(self) -> None:
        pass

    """Module functionality"""

    @kernel
    def set_mu(self, voltages: TList(TInt32)):
        assert len(voltages) == self.NUM_CHANNELS, 'Number of voltages given does not match number of channels'
        for v in voltages:
            assert self.voltage_to_mu(self.MIN_VOLTAGE) <= v <= self.voltage_to_mu(self.MAX_VOLTAGE), \
                'Voltage out of range'

        # Insert delay for slack
        delay(1 * ms * self.NUM_CHANNELS)
        # Set DAC
        self._dac.set_dac_mu(voltages, channels=list(range(self.NUM_CHANNELS)))

    @kernel
    def set(self, voltages: TList(TFloat)):
        self.set_mu([self.voltage_to_mu(v) for v in voltages])

    @portable
    def voltage_to_mu(self, voltage: TFloat) -> TInt32:
        return self._dac.voltage_to_mu(voltage)

    @kernel
    def reset(self):
        """Load last saved voltage settings."""
        self.set(self._voltages)

    @host_only
    def save_voltages(self, voltages: typing.List[float]) -> None:
        """Save voltage setting.

        :param voltages: A list of voltages to save
        """
        assert isinstance(voltages, list), 'Voltages must be a list'
        assert all(isinstance(v, float) for v in voltages), 'All voltages must be of type  float'
        assert len(voltages) == self.NUM_CHANNELS, 'Number of voltages given does not match number of channels'
        assert all(self.MIN_VOLTAGE <= v <= self.MAX_VOLTAGE for v in voltages), 'All voltages must be in range'
        self.set_dataset_sys(self.VOLTAGES_KEY, voltages)

    @host_only
    def get_saved_voltages(self) -> typing.List[float]:
        """Return the saved voltages (includes fallback and guarantees correct length)."""

        # Get voltages with fallback
        voltages: typing.List[float] = self.get_dataset_sys(self.VOLTAGES_KEY, fallback=[0 * V] * self.NUM_CHANNELS)
        # Calculate if there is a difference
        diff = self.NUM_CHANNELS - len(voltages)

        if diff > 0:
            # Extend
            voltages.extend([0 * V] * diff)
        elif diff < 0:
            # Trim
            voltages = voltages[:self.NUM_CHANNELS]

        assert len(voltages) == self.NUM_CHANNELS
        return voltages


class TrapModule(DaxModule):

    def build(self):
        # Submodules
        self.dc = TrapDcModule(self, 'dc')
        self.update_kernel_invariants('dc')

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass
