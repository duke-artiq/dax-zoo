from dax.experiment import *
from artiq.coredevice.ad9910 import AD9910, RAM_MODE_CONT_RAMPUP, RAM_DEST_FTW
import numpy as np


class TickleModule(DaxModule):
    """
    The tickle electrodes component of the trap.
    """
    K_RESONANT_FREQ_KEY = 'k_resonant_frequency'
    PHASE_KEY = 'phase'
    AMP_KEY = 'amplitude'
    ATT_KEY = 'attenuation'

    def build(self):
        self.logger.debug('Initiating trap tickle electrodes.')

        # DDS for trap tickle electrodes
        self._tickle_electrodes = self.get_device('urukul1_ch1', AD9910)

        self.update_kernel_invariants('_tickle_electrodes')

    def init(self, *, force: bool = False) -> None:
        # Get dataset values
        self._k_resonant_freq: float = self.get_dataset_sys(self.K_RESONANT_FREQ_KEY, 0.117 * MHz)
        self._phase: float = self.get_dataset_sys(self.PHASE_KEY, 0.0)
        self._amp: float = self.get_dataset_sys(self.AMP_KEY, 1.0)
        self._att: float = self.get_dataset_sys(self.ATT_KEY, 0 * dB)
        # Update kernel invariants
        self.update_kernel_invariants('_k_resonant_freq', '_phase', '_amp', '_att')

        if force:
            # Initialize devices
            self.init_kernel()

    def post_init(self) -> None:
        pass

    @kernel
    def init_kernel(self):
        self.core.reset()

        # set initial parameters for ram mode
        self._tickle_electrodes.set_amplitude(self._amp)
        self._tickle_electrodes.set_phase(self._phase)

        self.disable_sweep_k()

        self.reset()

        self.core.wait_until_mu(now_mu())

    """Module functionality"""

    @host_only
    def config_k_sweep(self, span, step, time, center_freq=None):
        if center_freq is None:
            center_freq = self._k_resonant_freq
        freqs = np.arange(center_freq - span, center_freq + span, step)
        num_steps = np.int32(2 * span / step)
        freqs_to_ram = [0] * num_steps
        self._tickle_electrodes.frequency_to_ram(freqs, freqs_to_ram)
        freqs_to_ram = [np.int32(freq_ram) for freq_ram in freqs_to_ram]
        time_per_scan = time / num_steps
        step_for_ram = self._tickle_electrodes.sysclk * time_per_scan / 4
        if step_for_ram < 1:
            raise ValueError('Step size cannot be less than 4 * the DDS sysclk period '
                             '(4 ns for a typical 1 GHz sysclk)')
        step_for_ram = np.int32(round(step_for_ram))
        return num_steps, step_for_ram, freqs_to_ram

    @kernel
    def enable_sweep_k(self, args_for_sweep: TTuple([TInt32, TInt32, TList(TInt32)])):  # noqa ATQ309
        # Write to ram for frequency sweep

        # Unpack arguments
        (num_steps, step_for_ram, freqs_to_ram) = args_for_sweep

        # write ram profile 0
        self._tickle_electrodes.set_cfr1(ram_enable=0)
        self._tickle_electrodes.cpld.io_update.pulse_mu(8)
        self._tickle_electrodes.cpld.set_profile(0)

        # RAM_MODE_CONT_RAMPUP repeats it indefinitely
        self._tickle_electrodes.set_profile_ram(start=0, end=num_steps - 1, step=step_for_ram,
                                                nodwell_high=1, mode=RAM_MODE_CONT_RAMPUP)
        self._tickle_electrodes.cpld.io_update.pulse_mu(8)
        self._tickle_electrodes.write_ram(freqs_to_ram)

        # configure registers for ram mode
        self._tickle_electrodes.set_cfr1(ram_enable=1, ram_destination=RAM_DEST_FTW)

        # start playback
        self._tickle_electrodes.cpld.io_update.pulse_mu(8)
        self._tickle_electrodes.sw.on()

    @kernel
    def disable_sweep_k(self):
        # Disable the ram mode
        self._tickle_electrodes.sw.off()
        self._tickle_electrodes.set_cfr1(ram_enable=0)
        self._tickle_electrodes.cpld.io_update.pulse_mu(8)

    @kernel
    def set(self, state: TBool):
        """Set the state of the microwave switch.

        :param state: `True` to enable the microwave switch
        """
        self._tickle_electrodes.sw.set_o(state)

    @kernel
    def on(self):
        """Enable the microwave switch."""
        self.set(True)

    @kernel
    def off(self):
        """Disable the microwave switch."""
        self.set(False)

    @kernel
    def pulse_mu(self, duration: TInt64):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in machine units
        """
        try:
            self.on()
            delay_mu(duration)
            self.off()
        except RTIOUnderflow:
            self.safety_off()
            self.core.wait_until_mu(now_mu())
            raise

    @kernel
    def pulse(self, duration: TFloat):
        """Apply a pulse with a given duration.

        :param duration: The duration of the pulse in seconds
        """
        self.pulse_mu(self.core.seconds_to_mu(duration))

    @kernel
    def safety_off(self):
        """Switch laser off in a safe manner.

        This function is slow and should not raise any exceptions.
        """
        self.core.break_realtime()
        self.off()
        self.core.wait_until_mu(now_mu())

    @kernel
    def config_att(self, att: TFloat):
        """Configures the attenuation used in the trap.

        :param att: Attenuation of sinusoidal signal
        """

        delay(100 * us)

        self._tickle_electrodes.set_att(att)

    @kernel
    def config(self, freq: TFloat, phase: TFloat = 0.0, amp: TFloat = 1.0):
        """Configures the  frequency used in the trap module. Frequency and phase will be converted to machine units

        :param freq: The frequency of the signal
        :param phase: The phase offset of the signal
        :param amp: The amplitude of the signal
        """
        self.config_mu(ftw=self._tickle_electrodes.frequency_to_ftw(freq),
                       pow_=self._tickle_electrodes.turns_to_pow(phase),
                       asf=self._tickle_electrodes.amplitude_to_asf(amp))

    @kernel
    def config_mu(self, ftw: TInt32, pow_: TInt32 = 0, asf: TInt32 = 0x3fff):
        """Configures the  frequency (in machine units) used in the trap module

        :param ftw: Frequency tuning word
        :param pow_: Phase offset word
        :param asf: Amplitude scale factor
        """

        # Add some slack
        delay(200 * us)

        self._tickle_electrodes.set_mu(ftw, pow_, asf)

    @kernel
    def reset(self):
        """Resets all hardware parameters to those last read from the dataset during the init phase
        """
        # Set to dataset settings
        self.config(self._k_resonant_freq, self._phase, self._amp)
        self.config_att(self._att)

    @host_only
    def save_amp(self, amp: float):
        """Saves amplitude into the dataset for future initialization.

        :param amp: The amplitude of the signal
        """
        assert isinstance(amp, float), 'Amplitude must be of type float'
        assert 0.0 <= amp <= 1.0, 'Amplitude out of range'
        self.set_dataset_sys(self.AMP_KEY, amp)

    @host_only
    def save_k_resonant_freq(self, k_resonant_freq: float):
        """Saves K resonant frequency into the dataset for future initialization.

        :param k_resonant_freq: The lower limit for frequency range of the signal
        """
        assert isinstance(k_resonant_freq, float), 'K resonant frequency must be of type float'

        # DDS frequencies typically go upto 400 MHz, but due to a mod to this AD9910, performance degrades after 10 MHz
        assert 0.0 * MHz <= k_resonant_freq <= 10.0 * MHz, 'K resonant frequency out of range'
        self.set_dataset_sys(self.K_RESONANT_FREQ_KEY, k_resonant_freq)

    @host_only
    def save_att(self, att: float):
        """Saves attenuation into the dataset for future initialization.

        :param att: Attenuation of sinusoidal signal
        """
        assert isinstance(att, float), 'Attenuation must be of type float'
        assert 0.0 * dB <= att <= 31.0 * dB, 'Attenuation out of range'
        self.set_dataset_sys(self.ATT_KEY, att)
