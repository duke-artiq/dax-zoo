from dax.experiment import *
from dax.modules.hist_context import HistogramContext

from motion.modules.pmt import PmtModule


class StateService(DaxService):
    SERVICE_NAME = 'state'

    def build(self) -> None:
        # Obtain required modules
        self._pmt = self.registry.find_module(PmtModule)
        self.update_kernel_invariants('_pmt')

        # Create histogram context
        self._histogram_context = HistogramContext(self, 'histogram',
                                                   plot_base_key='{scheduler.rid}', plot_group_base_key='')
        self.update_kernel_invariants('histogram', '_histogram_context')

    @host_only  # Added to prevent confusion with other functions
    def init(self) -> None:
        pass

    @host_only  # Added to prevent confusion with other functions
    def post_init(self) -> None:
        pass

    """Service functionality"""

    @property
    def histogram(self) -> HistogramContext:
        """Return the histogram context object.

        This context can be used with a `with` statement.
        Only inside this context it is possible to use the
        :func:`count` and :func:`measure` related functions.

        This context can be used inside or outside kernel context
        and relies on async RPC calls for enter and exit.

        The histogram context can be further configured by calling its functions.

        :return: The histogram context object
        """
        return self._histogram_context

    @kernel
    def detect_mu(self, duration: TInt64 = 0):
        """Detection (Ca PMT only).

        Call is forwarded to PMT module.

        :param duration: Duration of detection in machine units, default value if none given
        """
        self._pmt.ca.detect_mu(duration)

    @kernel
    def detect(self, duration: TFloat = 0.0):
        """Detection (Ca PMT only).

        Call is forwarded to PMT module.

        :param duration: Duration of detection, default value if none given
        """
        self._pmt.ca.detect(duration)

    @kernel
    def count(self):
        """Record a PMT count (Ca PMT only).

        The count value is requested from the PMT module and
        the result is stored in the histogram buffer.
        """

        # Append the detection count to the histogram buffer
        self.histogram.append([self._pmt.ca.count()])  # Make it a list for data uniformity
