import numpy as np

from dax.experiment import *

from motion.modules.laser import LaserModule
from motion.modules.pmt import PmtModule


class PulseService(DaxService):
    SERVICE_NAME = 'pulse'

    # System dataset keys
    PULSE_DIVIDER_KEY = 'pulse_divider'

    def build(self):
        # Get modules
        self._laser = self.registry.find_module(LaserModule)
        self._pmt = self.registry.find_module(PmtModule)
        self.update_kernel_invariants('_laser', '_pmt')

    def init(self) -> None:
        # Default pulse divider
        self._pulse_divider: int = self.get_dataset_sys(self.PULSE_DIVIDER_KEY, 400)
        self.update_kernel_invariants('_pulse_divider')
        assert self._pulse_divider > 0, 'Pulse divider must be greater than zero'

    def post_init(self) -> None:
        # Default detection time
        self._detection_time = self._pmt.get_default_detection_time()
        self.update_kernel_invariants('_detection_time')

    """Service functionality"""

    @kernel
    def pulse_and_count_mu(self, num_counts: TInt32, pulse_divider: TInt32 = 0,
                           detection_window: TInt64 = 0, detection_delay: TInt64 = 0,
                           gap: TInt64 = 0, disable_ca: TBool = False,
                           buffer_size: TInt32 = PmtModule.DEFAULT_BUFFER_SIZE,
                           setup: TBool = True, cleanup: TBool = True):
        """Do a high-performance PMT count for a given number of times and pulse in parallel.

        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param num_counts: The number of counts to perform (i.e. the number of detection windows)
        :param pulse_divider: Divider for the pulse period: ``pulse period = 2 * ((window + delay) / divider))``
          (default if none is given)
        :param detection_window: The detection window in machine units (default if none is given)
        :param detection_delay: The detection delay in machine units (i.e. delay between detection windows)
        :param gap: The time between switching lasers off and on in machine units
        :param disable_ca: Override flag to disable the Ca laser (set to :const:`True` to keep Ca laser off)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        """
        if pulse_divider <= 0:
            # Use default pulse divider
            pulse_divider = self._pulse_divider
        if detection_window <= 0:
            # Use default duration
            detection_window = self.core.seconds_to_mu(self._detection_time)

        # Limit values
        buffer_size = min(min(max(0, buffer_size), num_counts), self._pmt.MAX_BUFFER_SIZE)

        # Calculate the toggle period of the pulse (toggle period = pulse period / 2)
        toggle_period = max(np.int64(np.int64(detection_window + detection_delay) // pulse_divider),
                            np.int64(self.core.ref_multiplier))
        toggle_delay = max(np.int64(toggle_period - gap), np.int64(self.core.ref_multiplier))
        # Calculate the number of toggles in the detection window and the detection delay
        num_toggle_window = np.int32(detection_window / toggle_period)
        num_toggle_delay = pulse_divider - num_toggle_window

        # Set initial state of lasers
        state = True  # Initial state
        self._laser.toggle_gap_mu(state, gap=gap, disable_ca=disable_ca)

        if setup:
            # Build up a buffer
            for _ in range(buffer_size):
                # Gate and toggle
                for num_toggles, gate in [(num_toggle_window, True), (num_toggle_delay, False)]:
                    self._pmt.gate(gate)
                    if num_toggles > 0:
                        for _ in range(num_toggles):
                            # Toggle
                            delay_mu(toggle_delay)
                            state = not state
                            self._laser.toggle_gap_mu(state, gap=gap, disable_ca=disable_ca)
                    else:
                        delay_mu(np.int64(self.core.ref_multiplier))

        # Body
        for _ in range(num_counts - buffer_size):
            # Gate and toggle
            for num_toggles, gate in [(num_toggle_window, True), (num_toggle_delay, False)]:
                self._pmt.gate(gate)
                if num_toggles > 0:
                    for _ in range(num_toggles):
                        # Toggle
                        delay_mu(toggle_delay)
                        state = not state
                        self._laser.toggle_gap_mu(state, gap=gap, disable_ca=disable_ca)
                else:
                    delay_mu(np.int64(self.core.ref_multiplier))
            # Store PMT counts
            self._pmt.store()

        # Switch off lasers
        self._laser.off()

        if cleanup:
            # Clear out buffer
            for _ in range(buffer_size):
                # Store PMT counts
                self._pmt.store()

    @kernel
    def pulse_and_count(self, num_counts: TInt32, pulse_divider: TInt32 = 0,
                        detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0,
                        gap: TFloat = 0.0, disable_ca: TBool = False,
                        buffer_size: TInt32 = PmtModule.DEFAULT_BUFFER_SIZE,
                        setup: TBool = True, cleanup: TBool = True):
        """Do a high-performance PMT count for a given number of times and pulse in parallel.

        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param num_counts: The number of counts to perform (i.e. the number of detection windows)
        :param pulse_divider: Divider for the pulse period: ``pulse period = 2 * ((window + delay) / divider))``
          (default if none is given)
        :param detection_window: The detection window in seconds (default if none is given)
        :param detection_delay: The detection delay in seconds (i.e. delay between detection windows)
        :param gap: The time between switching lasers off and on in seconds
        :param disable_ca: Override flag to disable the Ca laser (set to :const:`True` to keep Ca laser off)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        """
        self.pulse_and_count_mu(num_counts=num_counts,
                                pulse_divider=pulse_divider,
                                detection_window=self.core.seconds_to_mu(detection_window),
                                detection_delay=self.core.seconds_to_mu(detection_delay),
                                gap=self.core.seconds_to_mu(gap),
                                disable_ca=disable_ca,
                                buffer_size=buffer_size,
                                setup=setup,
                                cleanup=cleanup)

    @kernel
    def pulse_and_count_window_mu(self, total_time: TInt64, pulse_divider: TInt32 = 0,
                                  detection_window: TInt64 = 0, detection_delay: TInt64 = 0,
                                  gap: TInt64 = 0, disable_ca: TBool = True,
                                  buffer_size: TInt32 = PmtModule.DEFAULT_BUFFER_SIZE,
                                  setup: TBool = True, cleanup: TBool = True) -> TInt32:
        """Do a high-performance PMT count for a given number of times and pulse in parallel.

        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param total_time: The total pulse and count time in machine units (number of windows will be **round up**)
        :param pulse_divider: Divider for the pulse period: ``pulse period = 2 * ((window + delay) / divider))``
          (default if none is given)
        :param detection_window: The detection window in machine units (default if none is given)
        :param detection_delay: The detection delay in machine units (i.e. delay between detection windows)
        :param gap: The time between switching lasers off and on in machine units
        :param disable_ca: Override flag to disable the Ca laser (set to :const:`True` to keep Ca laser off)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        :return: Number of counts performed
        """
        if detection_window <= 0:
            # Use default duration
            detection_window = self.core.seconds_to_mu(self._detection_time)

        # Limit values
        detection_delay = max(np.int64(self.core.ref_multiplier), detection_delay)

        # Convert time to number of counts
        num_counts = np.int32(total_time // (detection_window + detection_delay))
        if total_time % (detection_window + detection_delay):
            num_counts += 1  # Round up

        self.pulse_and_count_mu(num_counts=num_counts,
                                pulse_divider=pulse_divider,
                                detection_window=detection_window,
                                detection_delay=detection_delay,
                                gap=gap,
                                disable_ca=disable_ca,
                                buffer_size=buffer_size,
                                setup=setup,
                                cleanup=cleanup)
        return num_counts

    @kernel
    def pulse_and_count_window(self, total_time: TFloat, pulse_divider: TInt32 = 0,
                               detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0,
                               gap: TFloat = 0.0, disable_ca: TBool = True,
                               buffer_size: TInt32 = PmtModule.DEFAULT_BUFFER_SIZE,
                               setup: TBool = True, cleanup: TBool = True) -> TInt32:
        """Do a high-performance PMT count for a given number of times and pulse in parallel.

        Note, this function returns with negative slack by default.
        Cleanup can be disabled, but the user is responsible for clearing buffers later!

        :param total_time: The total pulse and count time in seconds (number of windows will be **round up**)
        :param pulse_divider: Divider for the pulse period: ``pulse period = 2 * ((window + delay) / divider))``
          (default if none is given)
        :param detection_window: The detection window in seconds (default if none is given)
        :param detection_delay: The detection delay in seconds (i.e. delay between detection windows)
        :param gap: The time between switching lasers off and on in seconds
        :param disable_ca: Override flag to disable the Ca laser (set to :const:`True` to keep Ca laser off)
        :param buffer_size: The size of the buffer (larger buffer means higher performance, but also higher latency)
        :param setup: Perform setup by building up a buffer
        :param cleanup: Perform cleanup by clearing buffers (causes function to return with negative slack)
        :return: Number of counts performed
        """
        return self.pulse_and_count_window_mu(total_time=self.core.seconds_to_mu(total_time),
                                              pulse_divider=pulse_divider,
                                              detection_window=self.core.seconds_to_mu(detection_window),
                                              detection_delay=self.core.seconds_to_mu(detection_delay),
                                              gap=self.core.seconds_to_mu(gap),
                                              disable_ca=disable_ca,
                                              buffer_size=buffer_size,
                                              setup=setup,
                                              cleanup=cleanup)
