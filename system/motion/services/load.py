from dax.experiment import *

from motion.modules.laser import LaserModule
from motion.modules.pmt import PmtModule
from motion.modules.ionizing_shutter import IonizingShutterModule
from motion.modules.oven import OvenModule
from motion.modules.mot import MotModule


class LoadService(DaxService):
    SERVICE_NAME = 'load'

    def build(self):
        # Get modules
        self._laser = self.registry.find_module(LaserModule)
        self._pmt = self.registry.find_module(PmtModule)
        self._ionization_shutter = self.registry.find_module(IonizingShutterModule)
        self._oven = self.registry.find_module(OvenModule)
        self._mot = self.registry.find_module(MotModule)
        self.update_kernel_invariants('_laser', '_pmt', '_ionization_shutter', '_oven', '_mot')

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass

    """Service functionality"""

    @kernel
    def _ca(self, threshold: TInt32, detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0):
        """Generic loading procedure for Ca."""
        self._mot.ion_ca_mf_on()

        with self._oven:
            # Guarantee slack
            self.core.break_realtime()
            delay(1 * ms)

            # Set lasers
            self._laser.k1.off()
            self._laser.k2.off()
            self._laser.ca.on()

            with self._ionization_shutter:
                with self._pmt:
                    # Wait for ions to load
                    self._pmt.count_threshold(threshold, detection_window=detection_window,
                                              detection_delay=detection_delay)

                # Regain slack before switching off ionization shutter
                self.core.break_realtime()

        # TODO: store number of ions (future work)

    @kernel
    def ca_threshold(self, threshold: TInt32,
                     detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0):
        # Configure laser
        self._laser.ca.load_ca()
        # Loading procedure
        self._ca(threshold, detection_window=detection_window, detection_delay=detection_delay)

    @kernel
    def ca_threshold_config(self, threshold: TInt32,
                            ca_freq: TFloat, ca_amp: TFloat,
                            detection_window: TFloat = 0.0, detection_delay: TFloat = 0.0):
        # Configure laser
        self._laser.ca.config(ca_freq, amp=ca_amp)
        # Loading procedure
        self._ca(threshold, detection_window=detection_window, detection_delay=detection_delay)
