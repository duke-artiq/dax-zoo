import logging

from dax.experiment import *

from dax.modules.led import LedModule
from dax.modules.rpc_benchmark import RpcBenchmarkModule
from dax.modules.cpld_init import CpldInitModule

from motion.modules.beatnote import BeatnoteModule
from motion.modules.pmt import PmtModule
from motion.modules.laser import LaserModule
from motion.modules.ionizing_shutter import IonizingShutterModule
from motion.modules.trap import TrapModule
from motion.modules.oven import OvenModule
from motion.modules.tof import TofModule
from motion.modules.mot import MotModule
from motion.modules.tickle import TickleModule

from motion.services.load import LoadService
from motion.services.pulse import PulseService
from motion.services.state import StateService


class MotionSystem(DaxSystem):
    SYS_ID = 'motion'
    SYS_VER = 1

    def build(self) -> None:
        # Adjust logging level
        self.logger.setLevel(min(self.logger.getEffectiveLevel(), logging.INFO))

        # Call super
        super(MotionSystem, self).build()

        # Get scheduler
        self.scheduler = self.get_device('scheduler')
        self.update_kernel_invariants('scheduler')

        # Add standard modules
        RpcBenchmarkModule(self, 'rpc_bench')
        self.led = LedModule(self, 'led', *['led0', 'led1'], init_kernel=False)
        self.cpld = CpldInitModule(self, 'cpld_init', init_kernel=False)
        self.update_kernel_invariants('led', 'cpld')

        # Add custom modules
        BeatnoteModule(self, 'beatnote')
        self.pmt = PmtModule(self, 'pmt')
        self.laser = LaserModule(self, 'laser')
        self.ionizing_shutter = IonizingShutterModule(self, 'ionizing_shutter')
        self.trap = TrapModule(self, 'trap')
        self.oven = OvenModule(self, 'oven')
        self.tof = TofModule(self, 'tof')
        self.mot = MotModule(self, 'mot')
        self.tickle = TickleModule(self, 'tickle')
        self.update_kernel_invariants('pmt', 'laser', 'ionizing_shutter', 'trap', 'oven', 'tof', 'mot', 'tickle')

        # Add custom services
        self.load = LoadService(self)
        self.pulse = PulseService(self)
        self.state = StateService(self)
        self.update_kernel_invariants('load', 'pulse', 'state')

    @kernel
    def init(self):
        """Joint kernel to initialize various modules.

        By manually initializing modules in a single kernel, the number of compiler runs
        can be reduced with faster initialization as a result.
        """
        # Call initialization kernel functions (they include calls to reset() and wait_until_mu()
        self.led.init_kernel()
        self.cpld.init_kernel()
        self.pmt.init_kernel()
        self.laser.init_kernel()
        self.ionizing_shutter.init_kernel()
        self.tof.init_kernel()
        self.tickle.init_kernel()

        # Reset CPLD switches used by legacy code
        for cpld in self.cpld.cpld:
            self.core.break_realtime()
            cpld.cfg_switches(0x0)
        self.core.wait_until_mu(now_mu())
