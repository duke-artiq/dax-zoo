# Motion

Code in this directory was originally developed for the MOTion system at Duke. The MOTion system uses a hybrid ion-atom
trap for studying interactions between atomic and molecular ions and neutral atoms.

Authors:

- Leon Riesebos
- Aniket Dalvi

Year: 2022
