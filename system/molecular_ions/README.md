# Molecular ions

Code in this directory was originally developed for the Molecular ions system at Duke. This project investigates a
dipole-phonon interaction between a trapped calcium oxide molecular ion and its harmonic motion. This interaction may
allow for the possibility of state preparation and measurement of quantum information encoded in a molecular ion qubit.

Authors:

- Filip Mazurek
- Travis Hurant
- Leon Riesebos

Year: 2022
