from typing import Tuple

from dax.experiment import *
from dax.modules.safety_context import SafetyContext


class OvenModule(DaxModule):
    """
    The oven is powered by RPCs to an Agilent E3631 power supply.
    """

    def build(self):
        self._oven_context = SafetyContext(self, 'oven_context', enter_cb=self.on, exit_cb=self.off)

        self._power_supply = self.get_device('power_supply')
        # Set the power supply output to the 6V output.
        # The voltage range is 0 - 6.18V
        # The current range is 0 - 5.25A
        self._power_supply_output = 'P6V'

    def init(self) -> None:
        """Initialize this module."""
        # Every time an experiment is initialized, ensure that the correct oven driver is set up
        self.check_connection()

    def post_init(self) -> None:
        pass

    @property
    def context(self):
        """Returns the oven context. The oven is turned on when entering the context and turned off when exiting the
        context. """
        return self._oven_context

    def check_voltage(self, voltage: float):
        assert 0 <= voltage <= 6.18, 'Voltage is out of range. Valid voltage range is 0 - 6.18V'

    def check_current(self, current: float):
        assert 0 <= current <= 5.25, 'Current is out of range. Valid current range is 0 - 5.25A'

    def on(self) -> None:
        """Turns on the oven by ensuring the power supply outputs are enabled."""
        self._power_supply.enable_outputs(True)

    def set(self, voltage: float, current: float = None) -> None:
        """Adjusts the oven temperature by passing the given voltage and current values to the power supply.

        :param voltage: The voltage that should be set on the output of the power supply. Range is 0 - 6.18V
        :param current: (Optional) The current that should be set on the output of the power supply. Range is 0 - 5.25A
        """
        self._power_supply.apply(self._power_supply_output, voltage, current)

    def off(self):
        """Turn off the oven."""
        self._power_supply.apply(self._power_supply_output, 0.0, 0.0)
        self._power_supply.enable_outputs(False)

    def get_output_settings(self) -> Tuple[float, float]:
        """Returns the voltage and current values of the power supply.

        :raises OvenError: Raised if the oven cannot communicate with the power supply
        """

        settings = self._power_supply.get_output_settings(self._power_supply_output)
        self.logger.info(f"Power supply output settings (raw value): {settings}.")
        try:
            return settings
        except Exception as e:
            self.logger.error("Unable to parse response from power supply.")
            raise OvenError("Unable to parse response from power supply.") from e

    def check_connection(self) -> None:
        """Runs silently if connected to the power supply, otherwise the driver throws as error.
        Also accepts if using the simulation or offline drivers
        """
        self._power_supply.check_connection()


class OvenError(RuntimeError):
    """Error raised when the oven cannot communicate with the power supply."""
    pass
