from artiq.coredevice.ad9910 import AD9910
from molecular_ions.coredevice.ad9912 import AD9912

from dax.experiment import *
from molecular_ions.modules.invariant_beam import _InvariantBeamModule


class _GenericBeamModule(_InvariantBeamModule):
    """
    Generic class for controlling DDSes. Allows configuring frequency and attenuation at runtime
    """

    """Module Functionality"""

    @kernel
    def config_attenuation(self, att: TFloat, realtime: TBool = False):
        """Configures the beam's attenuation.

        :param att: Attenuation of sinusoidal signal
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        # If this function is used a lot, then may be worthwhile to expose a mu function

        # Check safety
        if not self.is_valid_attenuation(att):
            raise ValueError('Beam aom attenuation must be within specified safe range.')

        if not realtime:
            delay(200 * us)

        self._beam.set_att(att)

    @kernel
    def config_frequency(self, freq: TFloat, phase: TFloat = 0.0, realtime: TBool = False):
        """Configures the beam's frequency. Frequency and phase will be converted to machine units. As a result,
        this function is not recommended for more than one-off use

        :param freq: The frequency of the individual beam
        :param phase: The phase offset of the sinusoidal signal
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        self.config_frequency_mu(self._beam.frequency_to_ftw(freq),
                                 self._beam.turns_to_pow(phase),
                                 realtime)

    @kernel
    def config_frequency_mu(self, ftw, pow_: TInt32 = 0, realtime: TBool = False):
        """Configures the  frequency (in machine units) used for the beam

        :param ftw: Frequency tuning word. TInt32 on an ad9910, TInt64 on an ad9912
        :param pow_: Phase offset word
        :param realtime: If true, timeline alterations are compensated and the caller is responsible for timing
        """
        # Check safety
        if not self.is_valid_frequency_mu(ftw):
            raise ValueError('Beam aom frequency must be within safe range.')

        if not realtime:
            delay(200 * us)

        self._beam.set_mu(ftw, pow_)


class AD9910GenericBeamModule(_GenericBeamModule):
    DEVICE_CLASS = AD9910


class AD9912GenericBeamModule(_GenericBeamModule):
    DEVICE_CLASS = AD9912
