from artiq.coredevice.ad9910 import AD9910
from molecular_ions.coredevice.ad9912 import AD9912

from dax.experiment import *


class _InvariantBeamModule(DaxModule):
    """
    Invariant class for controlling DDSes. Disallows changing frequency or attenuation after initialization.
    """
    # Class constants
    DEVICE_CLASS = None

    # System dataset keys
    ENABLED_KEY = 'enabled'
    FREQ_KEY = 'frequency'
    PHASE_KEY = 'phase'
    ATT_KEY = 'attenuation'

    def build(self, device_name: str, *, freq_min: float = 0 * Hz, freq_max: float = 400 * MHz,
              att_min: float = 0 * dB, att_max: float = 30 * dB, default_enabled: bool, default_freq: float,
              default_phase: float, default_att: float) -> None:
        self.logger.debug(f'Initializing module for {device_name}')

        # Correctness checks
        assert freq_min <= default_freq <= freq_max
        assert att_min <= default_att <= att_max

        # Obtain device
        self._beam = self.get_device(device_name, self.DEVICE_CLASS)
        # Add to kernel invariants
        self.update_kernel_invariants('_beam')

        # Save all limits for the DDS
        self._freq_min = freq_min
        self._freq_max = freq_max
        self._att_min = att_min
        self._att_max = att_max

        # Add to kernel invariants
        self.update_kernel_invariants('_freq_min', '_freq_max', '_att_min', '_att_max')

        # Save default values for use during initialization
        self._default_enabled = default_enabled
        self._default_freq = default_freq
        self._default_phase = default_phase
        self._default_att = default_att

        # Add to kernel invariants
        self.update_kernel_invariants('_default_enabled', '_default_freq', '_default_phase', '_default_att')

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # Convert frequency to ftw for use in validation function `is_valid_frequency_mu`
        self.ftw_min = self.frequency_to_ftw(self._freq_min)
        self.ftw_max = self.frequency_to_ftw(self._freq_max)
        # Add to kernel invariants
        self.update_kernel_invariants('ftw_min', 'ftw_max')

        # Default state values
        self._enabled_influx: bool = self.get_dataset_sys(self.ENABLED_KEY, self._default_enabled)
        freq_influx = self.get_dataset_sys(self.FREQ_KEY, self._default_freq)
        phase_mu_influx: int = self.get_dataset_sys(self.PHASE_KEY, self._default_phase)
        self._att_influx: float = self.get_dataset_sys(self.ATT_KEY, self._default_att)
        self.update_kernel_invariants('_enabled_influx', '_freq_mu_influx', '_phase_mu_influx', '_att_influx')

        # Convert default values to machine units
        self._freq_mu_influx = self.frequency_to_ftw(freq_influx)
        self._phase_mu_influx = self.turns_to_pow(phase_mu_influx)

        if force:
            # Initialize devices
            self.init_kernel()

    def post_init(self) -> None:
        # Only required when initialization is dependent on initialization of other modules (e.g. when using DMA)
        pass

    @kernel
    def init_kernel(self):
        # Give enough slack to run the initialization kernel
        self.core.reset()

        # Configure the beam to its desired parameters
        self.set_db_params()

        # Wait until all events are submitted
        self.core.wait_until_mu(now_mu())

    """Module Functionality"""

    @portable
    def frequency_to_ftw(self, freq: TFloat):
        """Convert frequency to machine units. No return typing given so that the correct type can be returned based on
         the underlying hardware

        :param freq: Frequency in Hertz
        :return: Converted frequency in machine units. ad9910 returns int32, ad9912 returns int64
        """
        # No return typing is given so that the correct type can be returned based on the underlying hardware
        return self._beam.frequency_to_ftw(freq)

    @portable
    def ftw_to_frequency(self, ftw) -> TFloat:
        """Convert ftw (frequency machine units) to frequency in Hertz. No param typing given so that the correct type
        can be returned based on the underlying hardware

        :param ftw: Frequency in machine units. ad9910 takes int32, ad9912 takes int64
        :return: Converted frequency in Hertz
        """
        # No parameter typing is given so that the underlying hardware accepts the necessary type
        return self._beam.ftw_to_frequency(ftw)

    @portable
    def turns_to_pow(self, phase: TFloat) -> TInt32:
        """Convert phase to machine units

        :param phase: Phase in radians
        :return: Converted phase in machine units
        """
        return self._beam.turns_to_pow(phase)

    @portable
    def pow_to_turns(self, pow: TInt32) -> TFloat:
        """Convert phase to radians

        :param pow: Phase in machine units
        :return: Phase in radians
        """
        return self._beam.pow_to_turns(pow)

    @portable
    def is_valid_frequency(self, freq: TFloat) -> TBool:
        """ Check if the given frequency is within the beam's allowed values

        :param freq: Frequency of the aom
        :raises ValueError: Raised if the frequency is out of range
        """
        return self._freq_min <= freq <= self._freq_max

    @portable
    def is_valid_frequency_mu(self, ftw) -> TBool:
        """ Check if the given frequency is within the beam's allowed values

        :param ftw: Frequency tuning word of the aom (machine units). TInt32 on an ad9910, TInt64 on an ad9912
        :raises ValueError: Raised if the frequency is out of range
        """
        return self.ftw_min <= ftw <= self.ftw_max

    @portable
    def is_valid_attenuation(self, att: TFloat) -> TBool:
        """ Check if the given attenuation is within the beam's allowed values

        :param att: Attenuation of the aom
        :raises ValueError: Raised if the attenuation is out of range
        """
        return self._att_min <= att <= self._att_max

    @kernel
    def set_o(self, state: TBool):
        """Set the state of the microwave switch.

        :param state: `True` to enable the microwave switch
        """
        self._beam.sw.set_o(state)

    @kernel
    def on(self):
        """Turn the microwave switch on
        """
        self.set_o(True)

    @kernel
    def off(self):
        """Turn the microwave switch off
        """
        self.set_o(False)

    @kernel
    def pulse(self, duration: TFloat):
        """ Turn the beam microwave switch on for a specified amount of time, then turn it off.

        :param duration: The amount of time for which the AOM should be enabled
        """
        self._beam.sw.pulse(duration)

    @kernel
    def set_db_params(self):
        """Configures the beam with the last loaded values from the datastore (influx).

        :raises ValueError: Raised if the frequency or attenuation is out of range
        """
        # Perform safety checks
        if not self.is_valid_frequency_mu(self._freq_mu_influx):
            raise ValueError('Beam aom frequency must be within safe range.')
        if not self.is_valid_attenuation(self._att_influx):
            raise ValueError('Beam aom attenuation must be within safe range.')

        # Set beam values
        self._beam.set_mu(self._freq_mu_influx, self._phase_mu_influx)  # Frequency
        # Setting attenuation takes a lot of time
        delay(200 * us)
        self._beam.set_att(self._att_influx)
        self.set_o(self._enabled_influx)


class AD9910InvariantBeamModule(_InvariantBeamModule):
    DEVICE_CLASS = AD9910


class AD9912InvariantBeamModule(_InvariantBeamModule):
    DEVICE_CLASS = AD9912
