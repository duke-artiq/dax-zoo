from numpy import int64

from dax.experiment import *

from molecular_ions.modules.generic_beam import AD9910GenericBeamModule, AD9912GenericBeamModule
from molecular_ions.modules.invariant_beam import AD9910InvariantBeamModule
from molecular_ions.modules.ionizing_shutter import IonizingShutterModule


class IndvBeamModule(DaxModule):
    """
    The AOMs for each cooling laser, plus a shutter for the ionizing beam
    729 nm: sideband cooling
    397 nm: Doppler cooling
    854 nm: sideband cooling
    417 nm: for future use
    423 nm: ionizing beam - controlled by shutter
    """
    # Constants for creating beam modules
    PARAMS_397 = {
        'device_name': 'urukul1_ch0',
        'freq_min': 85 * MHz,
        'freq_max': 135 * MHz,
        'att_min': 6 * dB,
        'att_max': 30 * dB,
        'default_enabled': True,
        'default_freq': 100 * MHz,
        'default_phase': 0.0,
        'default_att': 10 * dB
    }
    PARAMS_417 = {
        'device_name': 'urukul1_ch2',
        'freq_min': 85 * MHz,
        'freq_max': 135 * MHz,
        'att_min': 6 * dB,
        'att_max': 30 * dB,
        'default_enabled': False,
        'default_freq': 100 * MHz,
        'default_phase': 0.0,
        'default_att': 10 * dB
    }
    PARAMS_729 = {
        'device_name': 'urukul0_ch2',
        'freq_min': 185 * MHz,
        'freq_max': 215 * MHz,
        'att_min': 6 * dB,
        'att_max': 30 * dB,
        'default_enabled': True,
        'default_freq': 200 * MHz,
        'default_phase': 0.0,
        'default_att': 10 * dB
    }
    PARAMS_854 = {
        'device_name': 'urukul1_ch1',
        'freq_min': 190 * MHz,
        'freq_max': 220 * MHz,
        'att_min': 6 * dB,
        'att_max': 30 * dB,
        'default_enabled': True,
        'default_freq': 200 * MHz,
        'default_phase': 0.0,
        'default_att': 10 * dB
    }

    def build(self) -> None:
        self.logger.debug("Initiating indv_beam.")

        # Obtain devices
        # 9910 ch0:397; ch1: 854; ch2: 417
        self.cool_397 = AD9910GenericBeamModule(self, 'cool_397', **self.PARAMS_397)  # doppler cooling
        self.beam_417 = AD9910GenericBeamModule(self, 'beam_417',
                                                **self.PARAMS_417)  # for future experiments, molecular cooling
        # 854 parameters should never change, hence the invariant beam module
        self.cool_854 = AD9910InvariantBeamModule(self, 'cool_854', **self.PARAMS_854)  # sideband cooling
        # 9912 ch2: 729
        self.cool_729 = AD9912GenericBeamModule(self, 'cool_729', **self.PARAMS_729)  # sideband cooling

        # Add to kernel invariants
        self.update_kernel_invariants('cool_397', 'cool_854', 'beam_417', 'cool_729')

        # Lists of beams for quicker access
        self._beams_list_generic_ad9910 = [self.cool_397, self.beam_417]
        self._beams_list_invariant_ad9910 = [self.cool_854]
        self._beams_list_generic_ad9912 = [self.cool_729]
        # Add to kernel invariants
        self.update_kernel_invariants('_beams_list_generic_ad9910', '_beams_list_invariant_ad9910',
                                      '_beams_list_generic_ad9912')

        # Ionizing beam shutter as a submodule
        self.ionizing_beam_shutter = IonizingShutterModule(self, 'ionizing_beam_shutter')
        # Add to kernel invariants
        self.update_kernel_invariants('ionizing_beam_shutter')

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        # Initialization procedures to be executed at the start of experiments using dax_init()
        if force:
            # initialize devices
            self.init_kernel()

    @kernel
    def init_kernel(self):
        self.core.reset()

        # Initialize beams
        for beam0 in self._beams_list_generic_ad9910:
            beam0.init_kernel()

        for beam1 in self._beams_list_invariant_ad9910:
            beam1.init_kernel()

        for beam2 in self._beams_list_generic_ad9912:
            beam2.init_kernel()

        # Initialize ionizing shutter beam
        self.ionizing_beam_shutter.init_kernel()

        # Wait until current mu
        self.core.wait_until_mu(now_mu())

    def post_init(self) -> None:
        # Only required when initialization is dependent on initialization of other modules (e.g. when using DMA)
        pass

    """Module functionality"""

    @kernel
    def cooling_beams_off(self):
        """Switch all individual cooling beams off. This is the 397 nm, 729 nm, and 854 nm beams.
        Note: at this point the 417 nm beam is not considered part of the cooling beams.
        """
        self.cool_397.off()
        delay_mu(int64(self.core.ref_multiplier))
        self.cool_729.off()
        delay_mu(int64(self.core.ref_multiplier))
        self.cool_854.off()
        delay_mu(int64(self.core.ref_multiplier))

    @kernel
    def safety_all_beams_off(self):
        """Switch off all cooling beams and close the ionizing beam shutter
        """
        # Gain slack to have enough time to execute all events
        self.core.break_realtime()

        # Close ionization beam shutter
        self.ionizing_beam_shutter.close()

        # Turn off all beams
        for beam0 in self._beams_list_generic_ad9910:
            beam0.off()
            delay_mu(int64(self.core.ref_multiplier))

        for beam1 in self._beams_list_invariant_ad9910:
            beam1.off()
            delay_mu(int64(self.core.ref_multiplier))

        for beam2 in self._beams_list_generic_ad9912:
            beam2.off()
            delay_mu(int64(self.core.ref_multiplier))

        # Ensure all events are submitted
        self.core.wait_until_mu(now_mu())
