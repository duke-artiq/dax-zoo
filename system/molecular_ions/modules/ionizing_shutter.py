import artiq.coredevice.ttl  # type: ignore

from dax.experiment import *
from dax.modules.safety_context import SafetyContext

IONIZING_SHUTTER_KEY = "ionizing_shutter"


class IonizingShutterModule(DaxModule):
    """ Module to control a shutter driver. """

    # System dataset keys
    OPEN_KEY = "open"

    def build(self, init_kernel: bool = False) -> None:
        """Build the shutter for ionization laser module.

        :param init_kernel: Run initialization kernel during default module initialization
        """

        # Check arguments
        assert isinstance(init_kernel, bool), 'Init kernel flag must be of type bool'

        # Store attributes
        self._init_kernel: bool = init_kernel
        self.logger.debug(f'Init kernel: {self._init_kernel}')

        # set up shutter output (control of opening and closing the shutter)
        self._ionizing_shutter_out = self.get_device(IONIZING_SHUTTER_KEY, artiq.coredevice.ttl.TTLOut)
        self.update_kernel_invariants('_ionizing_shutter_out')

        # Context used to guarantee proper entry and exit control of ionizing laser shutter
        self._ionizing_shutter_context = SafetyContext(self, module_name='ionizing_shutter_context',
                                                       enter_cb=self.open, exit_cb=self.close)
        self.update_kernel_invariants('shutter_context', '_ionizing_shutter_context')

        self.logger.debug("Ionizing shutter module instantiated")

    def init(self, *, force: bool = False) -> None:
        """Initialize this module.

        :param force: Force full initialization
        """
        self._shutter_open: float = self.get_dataset_sys(self.OPEN_KEY, False)
        self.update_kernel_invariants('_shutter_open')

        if self._init_kernel or force:
            # Initialize the shutters if the init flag is set
            self.logger.debug('Running initialization kernel for ionizing_shutter')
            self.init_kernel()

    def post_init(self) -> None:
        pass

    @kernel
    def init_kernel(self):
        """Kernel function to initialize this module.
        This function is called automatically during initialization unless the user configured otherwise.
        In that case, this function has to be called manually.
        """
        # Reset the core
        self.core.reset()

        # Place shutter in specified state
        self.set_o(self._shutter_open)

        # Wait until event is submitted
        self.core.wait_until_mu(now_mu())

    @kernel
    def set_o(self, o: TBool):
        """Set the ionizing beam state to o.

        :param o: The state to which set the ionizing beam shutter. True=open, False=closed.
        """
        self._ionizing_shutter_out.set_o(o)

    @kernel
    def open(self):
        """Open the ionizing beam shutter."""
        self.set_o(True)

    @kernel
    def close(self):
        """Close the ionizing beam shutter."""
        self.set_o(False)

    @kernel
    def pulse(self, duration: TFloat):
        """Pule the ionizing beam shutter high for specified duration (in seconds), then close.

        :param duration: Amount of time for shutter to be open
        """
        self._ionizing_shutter_out.pulse(duration)

    @kernel
    def pulse_mu(self, duration: TInt64):
        """Pulse the output high for the specified duration (in machine units).
        The time cursor is advanced by the specified duration.

        :param duration: Amount of time for shutter to be open (in machine units).
        """
        self._ionizing_shutter_out.pulse_mu(duration)

    @property
    def shutter_context(self):
        """Return the ionizing shutter laser context.

        The shutter safety context is used with a `with` statement.
        The shutter is opened when entering the context and closed at exit.

        The context can be used inside and outside kernel context.
        We recommend using it in kernel context.

        :return: The ionizing shutter laser context
        """
        return self._ionizing_shutter_context
