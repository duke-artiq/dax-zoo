{ pkgs ? import <nixpkgs> { } }:

let
  flake8-artiq = pkgs.callPackage (import (builtins.fetchGit "https://gitlab.com/duke-artiq/flake8-artiq.git")) { inherit pkgs; };
in
pkgs.mkShell {
  buildInputs = [
    # Python packages
    (pkgs.python3.withPackages (ps: [
      flake8-artiq
    ]))
  ];
}
