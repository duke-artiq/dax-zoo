import typing
import types

from dax.experiment import *


class _PowerSwitch:
    """Private class encapsulating each channel of the power switch"""

    def __init__(self, power_switch, channel: int, name: str):
        # Get the power switch controller from Power Switch Module
        self._power_switch = power_switch
        # Channel name to registered
        self._channel = channel
        # Name of device to be registered on this channel
        self._name = name

    @rpc
    def on(self):
        # Switch on channel corresponding to this PowerSwitch object
        self._power_switch.switch_on(self._channel)

    @rpc
    def off(self):
        # Switch off channel corresponding to this PowerSwitch object
        self._power_switch.switch_off(self._channel)

    @property
    def channel(self):
        """Function to get channel"""
        return self._channel

    @property
    def name(self):
        """Function to get name"""
        return self._name


class PowerSwitchModule(DaxModule):
    """The Power Switch module is used to control the remote. It uses the Web power Switch by Digital Loggers, Inc."""
    MAX_CHANNELS = 8

    def build(self):
        # Get the power switch controller
        self._power_switch = self.get_device('power_switch')
        # Create map of channel number to device name
        self._channel_map: typing.Dict[int, _PowerSwitch] = {}

    @host_only
    def init(self) -> None:
        pass

    @host_only
    def post_init(self) -> None:
        pass

    """Module Functionality"""

    @host_only
    def register(self, channel: int, name: str = 'Generic Device'):
        """
        Function to register device on power switch

        :param channel: channel of device to be registered
        :param name: name of device to be registered
        """
        assert isinstance(channel, int)
        assert isinstance(name, str)

        # Check that the channel number is valid
        if not 1 <= channel <= self.MAX_CHANNELS:
            raise ValueError('Invalid channel number')
        # Check whether channel is already registered
        if channel in self._channel_map:
            raise RuntimeError('Channel already registered')

        # Create PowerSwitch object and add to registration dictionary
        self._channel_map[channel] = _PowerSwitch(self._power_switch, channel=channel, name=name)
        # Return created PowerSwitch object
        return self._channel_map[channel]

    @property
    def channel_map(self):
        """Function to get channel map (read only)."""
        return types.MappingProxyType(self._channel_map)
