from motion.system import *


class PowerSwitchFrontend(MotionSystem, Experiment):
    """Power switch frontend"""

    ON_KEY = 'on'
    OFF_KEY = 'off'
    DEFAULT_KEY = '<unchanged>'

    def build(self) -> None:
        # Call super
        super(PowerSwitchFrontend, self).build()

        # Get power switch module
        self.power_switch = self.registry.find_module(PowerSwitchModule)

        # Register all unregistered channels
        for i in range(1, self.power_switch.MAX_CHANNELS + 1):
            if i not in self.power_switch.channel_map:
                self.power_switch.register(i)

        # Arguments
        self.power_switch_channels = {
            switch: self.get_argument(
                f'{index} - {switch.name}',
                EnumerationValue([self.DEFAULT_KEY, self.ON_KEY, self.OFF_KEY], default=self.DEFAULT_KEY),
                tooltip='Set the channel to one of these states'
            )
            for index, switch in sorted(self.power_switch.channel_map.items())
        }

    def run(self):
        # Initialize the power switch module
        self.power_switch.init()
        self.power_switch.post_init()

        # Change state of channels based on arguments
        for switch, state in self.power_switch_channels.items():
            if state == self.ON_KEY:
                switch.on()
            elif state == self.OFF_KEY:
                switch.off()
