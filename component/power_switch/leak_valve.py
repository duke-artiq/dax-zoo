from dax.services.safety_context import SafetyContextService

from power_switch_module import PowerSwitchModule


class LeakValveService(SafetyContextService):
    """Module to control the valve shutter."""

    SERVICE_NAME = 'leak_valve'

    _VALVE_CHANNEL = 1
    """Power switch channel the valve is connected to."""

    def build(self) -> None:
        # Register channel for valve
        switch = self.registry.find_module(PowerSwitchModule).register(self._VALVE_CHANNEL)
        # Call super
        super(LeakValveService, self).build(enter_cb=switch.on, exit_cb=switch.off)

    def init(self) -> None:
        pass

    def post_init(self) -> None:
        pass
