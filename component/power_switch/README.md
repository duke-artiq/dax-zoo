# Power Switch

Code in this directory was originally developed for the power switch used in MOTion system at Duke. It has since been
removed from the system, but these components are still useful and transferable to other systems. The controller used to
for the power switch can be found [here](https://gitlab.com/duke-artiq/dax-comtools/-/blob/master/dax_comtools/frontend/aqctl_dli_wps.py), with the corresponding driver [here](https://gitlab.com/duke-artiq/dax-comtools/-/blob/master/dax_comtools/devices/dli_wps/driver.py).
These are in the `dax_comtools` repository.

Authors:

- Aniket Dalvi

Year: 2022
