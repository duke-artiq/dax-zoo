# Zotino module

Code in this directory was originally developed for the Zotino in MOTion system at Duke.
The Zotino is used for multiple purposes (e.g. trap DC, valve control), and needs to be shared in a safe way.
This module allows services to register channels on the Zotino to ensure there are no channel conflicts.
The registered channels are controlled through a proxy object that checks at runtime if the provided channels
are legal.

Authors:

- Leon Riesebos

Year: 2022
