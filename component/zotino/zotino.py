import typing

from dax.experiment import *

import artiq.coredevice.zotino


class _ZotinoProxy:
    """Zotino proxy object that mimics Zotino driver functions with an additional channel check.

    Users still call the channels by their absolute index.
    Channel index offsets are not calculated here since it would require extra integer operations.
    """

    kernel_invariants: typing.Set[str] = {'core', '_zotino', '_start', '_lenght', '_check'}

    def __init__(self, core, zotino: artiq.coredevice.zotino.Zotino, start: int, length: int, *,
                 check: bool = True):
        assert isinstance(start, int)
        assert 0 <= start < 32
        assert isinstance(length, int)
        assert 0 <= length < 32

        self.core = core
        self._zotino = zotino
        self._start = start
        self._lenght = length
        self._check = check

    @portable
    def _check_channels(self, channels: TList(TInt32)):
        if self._check:
            for c in channels:
                assert self._start <= c < self._start + self._lenght

    @kernel
    def set_dac_mu(self, values: TList(TInt32), channels: TList(TInt32)):
        assert len(values) <= len(channels)
        self._check_channels(channels)
        self._zotino.set_dac_mu(values, channels)

    @kernel
    def set_dac(self, voltages: TList(TFloat), channels: TList(TInt32)):
        self.set_dac_mu([self.voltage_to_mu(v) for v in voltages], channels)

    @portable
    def voltage_to_mu(self, voltage: TFloat) -> TInt32:
        return self._zotino.voltage_to_mu(voltage)


class ChannelError(Exception):
    pass


class ZotinoModule(DaxModule):

    def build(self, key: str) -> None:
        # Get Zotino device
        self._zotino = self.get_device(key, artiq.coredevice.zotino.Zotino)
        self.update_kernel_invariants('_zotino')

        # Set of reserved channels
        self._channels = set()

    @host_only
    def init(self) -> None:
        pass

    @host_only
    def post_init(self) -> None:
        pass

    @host_only
    def register(self, start: int, length: int, **kwargs: typing.Any) -> _ZotinoProxy:
        """Register Zotino channels and get a Zotino proxy object.

        :param start: The start channel
        :param length: The number of channels
        :param kwargs: Keyword arguments for the Zotino proxy object
        :returns: Zotino proxy object
        """
        channels = set(range(start, start + length))
        if channels & self._channels:
            raise ChannelError(f'Cannot register offset={start}, length={length}, not all channels are available')
        self._channels |= channels
        return _ZotinoProxy(self.core, self._zotino, start, length, **kwargs)
